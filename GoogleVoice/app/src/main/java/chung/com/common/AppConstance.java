package chung.com.common;

/**
 * Created by root on 7/27/16.
 */
public class AppConstance {
    public static final String KEY_CHOOSE_LOCATION = "choose_location";
    public static final int SPEECHINTENT_REQ_CODE=11;
    public static final int REQUEST_TO_LOCATION=12;
    public static final String KEY_LOCATION="location";
    public static final String KEY_POSITION = "position";
    public static final int REQUEST_CHOOSE_LOCATION = 13;
    public static final int REQUEST_CODE_CONTACT = 103;
    public static final int REQUEST_CODE_MESSAGE = 104;
    // Preferences
    public static final String PREF_REGISTRATION_ID = "pref_registration_id";

    // Intent
    public static final String INTENT_PUSH = "intent_push";
    public static final String INTENT_PUSH_MESSAGE = "intent_push_message";

    public static final String KEY_MES = "key_mes";
    public static final String INTERFACE_CLICK_JOB = "INTERFACE_CLICK_JOB";
    public static final int KEY_RESULT = 105;
    public static final String KEY_CONTACT = "KEY_CONTACT";

    public static final String KEYDATA = "KEYDATA";
    public static final String KEY_CONTACT_DETAIL = "KEY_CONTACT_DETAIL";
    public static final String KEY_CONTACT_1 = "KEY_CONTACT_1";
        public static final String KEY_PHONE ="KEY_PHONE" ;
    public static final String WEATHER_HUMI="Độ ẩm: ";
    public static final String WEATHER_SPEECH="Sức gió: ";
    public static final String WEATHER_DIR="Hướng gió: ";
    public static final String NORTH = "Hướng Bắc";
    public static final String NORT_EAST = "Hướng Đông Bắc";
    public static final String EAST = "Hướng Đông";
    public static final String EAST_SOUTH = "Hướng Đông Nam";
    public static final String SOUTH = "Hướng Nam";
    public static final String SOUTH_WEST = "Hướng Tây Nam";
    public static final String WEST = "Hướng Tây";
    public static final String WEST_NORTH = "Hướng Tây Bắc";


}
