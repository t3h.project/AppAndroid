package chung.com.data;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import chung.com.googlevoice.R;
import chung.com.model.Call;
import chung.com.model.Contact;
import chung.com.model.Sms;
import chung.com.model.Song;


/**
 * Created by chung on 5/23/16.
 */
public class Manager {
    public static final String[] COLUMNS = {"_id", "_data", "title", "_size", "duration", "artist", "album"};
    public static final int REQUEST_READ_STORAGE = 200;
    private Context context;
    private ContentResolver resolver;
    private ArrayList<Contact> list;
    private ArrayList<Song> listSong;

    public static final String[] COLUMNS_CALL = {"number", "type", "date", "duration","name"};


    public Manager(Context context) {
        this.context = context;
        resolver = context.getContentResolver();
    }


    public ArrayList<Contact> getContact() {
        list = new ArrayList<>();
        Cursor cursor = resolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            String name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String phoneNumber = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            list.add(new Contact(name, convertPhoneNumber(phoneNumber)));
            cursor.moveToNext();
        }
        cursor.close();
        Collections.sort(list, new Comparator<Contact>() {
            public int compare(Contact c1, Contact c2) {
                return c1.getName().compareTo(c2.getName());
            }
        });
        return list;
    }

    public ArrayList<Song> getSong() {
        listSong = new ArrayList<>();
        Cursor cursor = resolver.query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, COLUMNS, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            int id = cursor.getInt(0);
            String data = cursor.getString(1);
            String title = cursor.getString(2);
            float size = cursor.getFloat(3);
            float duration = cursor.getFloat(4);
            String artist = cursor.getString(5);
            String album = cursor.getString(6);
            listSong.add(new Song(id, data, title, artist, album, duration, size));
            cursor.moveToNext();
        }
        return listSong;
    }

    public ArrayList<Sms> getAllSms(String _phone) {
        ArrayList<Sms> lstSms = new ArrayList<Sms>();
        Uri message = Uri.parse("content://sms/");
        Cursor c = resolver.query(message, null, null, null, "date ASC");
        int totalSMS = c.getCount();
        if (c.moveToFirst()) {
            for (int i = 0; i < totalSMS; i++) {
                String _id = c.getString(c.getColumnIndexOrThrow("_id"));
                String address = c.getString(c.getColumnIndexOrThrow("address"));
                String body = c.getString(c.getColumnIndexOrThrow("body"));
                String read = c.getString(c.getColumnIndex("read"));
                String time = c.getString(c.getColumnIndexOrThrow("date"));
                String folder = "";
                if (c.getString(c.getColumnIndexOrThrow("type")).contains("1")) {
                    folder = "inbox";
                } else {
                    folder = "sent";
                }
                Sms sms = new Sms(_id, convertPhoneNumber(address), body, read, time, folder);
                if (sms.getAddress().equals(_phone)) {
                    lstSms.add(sms);
                }
                c.moveToNext();
            }
        }

        c.close();
        Collections.sort(lstSms, new Comparator<Sms>() {
            public int compare(Sms c1, Sms c2) {
                return c2.getId().compareTo(c1.getId());
            }
        });
        return lstSms;
    }

    public ArrayList<Call> getHistoryCall(String _phone) {
        ArrayList<Call> arr = new ArrayList<>();
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_CALL_LOG)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) context,
                    new String[]{android.Manifest.permission.READ_CALL_LOG, android.Manifest.permission.READ_CALL_LOG},
                    1);
        } else {
            Cursor cursor = resolver.query(CallLog.Calls.CONTENT_URI, COLUMNS_CALL, null, null, null);
            cursor.moveToFirst();

            while (!cursor.isAfterLast()) {
                int image = 0;
                String number = cursor.getString(0);
                int t = cursor.getInt(1);
                switch (t) {
                    case CallLog.Calls.OUTGOING_TYPE:
                        image = R.drawable.ic_call_out;
                        break;
                    case CallLog.Calls.INCOMING_TYPE:
                        image = R.drawable.ic_call_in;
                        break;
                    case CallLog.Calls.MISSED_TYPE:
                        image = R.drawable.ic_call_miss;
                        break;
                }
                long d = cursor.getLong(2);
                Date date = new Date(Long.valueOf(d));
                DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(context);
                long tc = cursor.getLong(3);
                String name = cursor.getString(4);
                int seconds = (int) (tc / 1000);
                String time = seconds / 60 + ":" + seconds % 60;
                Call c = new Call(image, convertPhoneNumber(number), time, dateFormat.format(date),name);
                if (_phone.endsWith(number)){
                    arr.add(c);
                }
                cursor.moveToNext();
            }
            cursor.close();

        }
        return arr;
    }

    public String convertPhoneNumber(String _phone) {
        String newPhone = "";
        try {
            String s = _phone.replaceAll(" ", "");
            if (s.contains("+84")) {
                newPhone = s.replace("+84", "0");
            } else if (s.contains("+")) {
                newPhone = s.replace("+", "");
            } else {
                newPhone = s;
            }
        } catch (Exception e) {
        }


        return newPhone;
    }


}
