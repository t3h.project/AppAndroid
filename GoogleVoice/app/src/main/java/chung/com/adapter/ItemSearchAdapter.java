package chung.com.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import chung.com.googlevoice.R;
import chung.com.model.ItemSearch;
import chung.com.onclick.IOnItemClickListener;

/**
 * Created by root on 8/1/16.
 */
public class ItemSearchAdapter extends RecyclerView.Adapter<ItemSearchAdapter.MyViewHolder> {

    private Context mContext;
    private ArrayList<ItemSearch> arrItem;
    private IOnItemClickListener mOnItemClickListener;
    public ItemSearchAdapter(Context context, ArrayList<ItemSearch> list) {
        mContext=context;
        arrItem=list;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_item_search,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ItemSearch itemSearch= arrItem.get(position);
        holder.tvTitle.setText(itemSearch.getTitle());
        holder.tvLink.setText(itemSearch.getLink());
        holder.tvDescription.setText(itemSearch.getDescription());

    }

    public void setmOnItemClickListener(IOnItemClickListener mOnItemClickListener) {
        this.mOnItemClickListener = mOnItemClickListener;
    }

    @Override
    public int getItemCount() {
        return arrItem.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvTitle, tvLink, tvDescription;
        public MyViewHolder(View itemView) {
            super(itemView);
            tvTitle= (TextView) itemView.findViewById(R.id.tv_title);
            tvLink= (TextView) itemView.findViewById(R.id.tv_link);
            tvDescription= (TextView) itemView.findViewById(R.id.tv_des);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mOnItemClickListener!=null){
                Log.e("Adapter: ",getAdapterPosition()+"");
                mOnItemClickListener.onClickItem(view,getAdapterPosition());
            }
        }
    }
}
