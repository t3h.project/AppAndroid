package chung.com.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import chung.com.callback.ItemClick;
import chung.com.common.AppConstance;
import chung.com.googlevoice.R;
import chung.com.model.Contact;

/**
 * Created by chung on 5/26/16.
 */
public class RVContact extends RecyclerView.Adapter<RVContact.RecyclerViewHolder> {

    private ArrayList<Contact> list = new ArrayList<>();
    private Context context;
    private ItemClick click;

    public RVContact(Context context, ArrayList<Contact> list) {
        this.context = context;
        this.list = list;
        click = (ItemClick) context;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.item_contact, viewGroup, false);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        final Contact contact = list.get(position);
        holder.tvContactName.setText(list.get(position).getName());
        holder.tvContactPhone.setText(list.get(position).getPhoneNo());
        holder.ll_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click.selectedItem(contact, AppConstance.INTERFACE_CLICK_JOB);
            }
        });
    }
    @Override
    public int getItemCount() {
        return list.size();
    }

    class RecyclerViewHolder extends RecyclerView.ViewHolder {
        TextView tvContactName, tvContactPhone;
        LinearLayout ll_layout;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            tvContactName = (TextView) itemView.findViewById(R.id.tvContactName);
            tvContactPhone = (TextView) itemView.findViewById(R.id.tvContactPhone);
            ll_layout = (LinearLayout) itemView.findViewById(R.id.ll_layout);
        }


    }
    public String convertPhoneNumber(String _phone) {
        String newPhone = "";
        try {
            String s = _phone.replaceAll(" ", "");
            if (s.contains("+84")) {
                newPhone = s.replace("+84", "0");
            } else if (s.contains("+")) {
                newPhone = s.replace("+", "");
            } else {
                newPhone = s;
            }
        } catch (Exception e) {
        }


        return newPhone;
    }

}
