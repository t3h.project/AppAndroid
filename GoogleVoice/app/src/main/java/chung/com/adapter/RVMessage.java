package chung.com.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import chung.com.googlevoice.R;
import chung.com.model.Sms;

/**
 * Created by chung on 8/1/16.
 */
public class RVMessage extends RecyclerView.Adapter<RVMessage.RecyclerViewHolder> {

    private ArrayList<Sms> list = new ArrayList<>();
    private Context context;

    public RVMessage(Context context, ArrayList<Sms> list) {
        this.context = context;
        this.list = list;

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.item_message, viewGroup, false);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        final Sms sms = list.get(position);
        long milliSeconds = Long.parseLong(sms.getTime());
        String dateString = new SimpleDateFormat("dd-MM-yyyy").format(new Date(milliSeconds));
        if (sms.getFolderName().equals("sent")) {
            holder.item_body_right.setText(sms.getMsg());
            holder.item_date_right.setText(dateString);
            holder.ll_item_left.setVisibility(View.GONE);

        } else {
            holder.item_body_left.setText(sms.getMsg());
            holder.item_date_left.setText(dateString);
            holder.ll_item_right.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class RecyclerViewHolder extends RecyclerView.ViewHolder {
        TextView item_date_left, item_body_left, item_date_right, item_body_right;
        LinearLayout ll_item_left, ll_item_right;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            item_date_left = (TextView) itemView.findViewById(R.id.item_date_left);
            item_body_left = (TextView) itemView.findViewById(R.id.item_body_left);
            item_date_right = (TextView) itemView.findViewById(R.id.item_date_right);
            item_body_right = (TextView) itemView.findViewById(R.id.item_body_right);
            ll_item_left = (LinearLayout) itemView.findViewById(R.id.ll_item_left);
            ll_item_right = (LinearLayout) itemView.findViewById(R.id.ll_item_right);
        }


    }


}

