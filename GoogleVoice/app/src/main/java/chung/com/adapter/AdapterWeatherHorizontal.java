package chung.com.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import chung.com.googlevoice.R;
import chung.com.model.Weather;

/**
 * Created by root on 8/3/16.
 */
public class AdapterWeatherHorizontal extends RecyclerView.Adapter<AdapterWeatherHorizontal.MyViewHolder> {

    private Context mContext;
    private ArrayList<Weather> arrWeather;

    public AdapterWeatherHorizontal(Context mContext, ArrayList<Weather> arrWeather) {
        this.mContext = mContext;
        this.arrWeather = arrWeather;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(mContext).inflate(R.layout.item_weather_detail_day,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Weather weather=arrWeather.get(position);
        holder.tvWeatherTime.setText(weather.currentCondition.getDate());
        holder.tvWeatherCelsius.setText(Math.round(weather.temperature.getTemp())+" \u00b0");
        String link="http://openweathermap.org/img/w/"+weather.currentCondition.getIcon()+".png";
        Picasso.with(mContext).load(link).into(holder.imgWeatherDay);
    }

    @Override
    public int getItemCount() {
        return arrWeather.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView tvWeatherTime, tvWeatherCelsius;
        ImageView imgWeatherDay;
        public MyViewHolder(View itemView) {
            super(itemView);
            tvWeatherTime= (TextView) itemView.findViewById(R.id.tv_weather_time_day);
            tvWeatherCelsius= (TextView) itemView.findViewById(R.id.tv_weather_cel_day);
            imgWeatherDay= (ImageView) itemView.findViewById(R.id.img_weather_day);
        }
    }
}
