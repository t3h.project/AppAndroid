package chung.com.adapter;

/**
 * Created by chung on 8/4/16.
 */

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import chung.com.googlevoice.R;
import chung.com.model.Call;

/**
 * Created by chung on 5/26/16.
 */
public class RVHistory extends RecyclerView.Adapter<RVHistory.RecyclerViewHolder> {

    private ArrayList<Call> list = new ArrayList<>();
    private Context context;


    public RVHistory(Context context, ArrayList<Call> list) {
        this.context = context;
        this.list = list;

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.item_history, viewGroup, false);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        final Call call = list.get(position);

        holder.tv_his_name.setText(list.get(position).getName());
        holder.tv_his_phone.setText(list.get(position).getNumber());
        holder.tv_his_date.setText(list.get(position).getDate());
        holder.iv_type_call.setImageResource(list.get(position).getImage());

    }
    @Override
    public int getItemCount() {
        return list.size();
    }

    class RecyclerViewHolder extends RecyclerView.ViewHolder {
        TextView tv_his_name, tv_his_phone,tv_his_date;
        ImageView iv_type_call;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            tv_his_name = (TextView) itemView.findViewById(R.id.tv_his_name);
            tv_his_phone = (TextView) itemView.findViewById(R.id.tv_his_phone);
            tv_his_date = (TextView) itemView.findViewById(R.id.tv_his_date);
            iv_type_call = (ImageView) itemView.findViewById(R.id.iv_type_call);
        }


    }
    public String convertPhoneNumber(String _phone) {
        String newPhone = "";
        try {
            String s = _phone.replaceAll(" ", "");
            if (s.contains("+84")) {
                newPhone = s.replace("+84", "0");
            } else if (s.contains("+")) {
                newPhone = s.replace("+", "");
            } else {
                newPhone = s;
            }
        } catch (Exception e) {
        }


        return newPhone;
    }

}
