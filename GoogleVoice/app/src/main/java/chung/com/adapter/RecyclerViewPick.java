package chung.com.adapter;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import chung.com.googlevoice.R;
import chung.com.model.Contact;

/**
 * Created by chung on 5/26/16.
 */
public class RecyclerViewPick extends RecyclerView.Adapter<RecyclerViewPick.RecyclerViewHolder> {

    public static final String KEY_PICK_CONTACT = "key_pick_contact";
    private ArrayList<Contact> list = new ArrayList<>();
    private Context context;

    public RecyclerViewPick(Context context, ArrayList<Contact> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        View view = inflater.inflate(R.layout.item_contact, viewGroup, false);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        holder.tvContactName.setText(list.get(position).getName());
        holder.tvContactPhone.setText(list.get(position).getPhoneNo());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvContactName, tvContactPhone;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            tvContactName = (TextView) itemView.findViewById(R.id.tvContactName);
            tvContactPhone = (TextView) itemView.findViewById(R.id.tvContactPhone);
        }

        @Override
        public void onClick(View v) {
            Fragment fragment = new Fragment();
            Bundle bundle = new Bundle();
            bundle.putString(KEY_PICK_CONTACT, tvContactPhone.getText().toString());
            fragment.setArguments(bundle);
           // MainActivity mainActivity = (MainActivity) context;
            //mainActivity.showFragmentMessage();
        }
    }
}
