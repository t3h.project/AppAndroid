package chung.com.dialog;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import chung.com.GPSTracker;
import chung.com.common.AppConstance;
import chung.com.googlevoice.R;
import chung.com.model.Weather;
import chung.com.parser.JSONWeatherParser;
import chung.com.utilities.Utilities;


/**
 * Created by root on 8/2/16.
 */
public class WeatherDialogFragment extends DialogFragment {

    public static WeatherDialogFragment f;
    private static Context mContext;
    private GPSTracker gpsTracker;
    private ImageView imgWeather;
    private static Utilities utilities;
    private static Geocoder mGeocoder;
    private TextView tvLocationName, tvDate, tvWeatherTemp, tvWeatherDes;
    private TextView tvWeatherHumi, tvWeatherSpeech, tvWeatherDir;


    public WeatherDialogFragment() {
        super();
    }

    public static WeatherDialogFragment newInstance(Context context) {
        mContext=context;
        utilities= new Utilities(context);
        mGeocoder= new Geocoder(context, Locale.getDefault());
       f= new WeatherDialogFragment();
        return f;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.layout_weather_dialog,container,false);
        imgWeather= (ImageView) v.findViewById(R.id.img_weather);

        tvLocationName= (TextView) v.findViewById(R.id.tv_location_name);
        tvDate= (TextView) v.findViewById(R.id.tv_date);
        tvWeatherTemp= (TextView) v.findViewById(R.id.tv_weather_tem);
        tvWeatherDes= (TextView) v.findViewById(R.id.tv_weather_des);
        tvWeatherHumi= (TextView) v.findViewById(R.id.tv_weather_humi);
        tvWeatherSpeech= (TextView) v.findViewById(R.id.tv_weather_speech);
        tvWeatherDir= (TextView) v.findViewById(R.id.tv_weather_dir);

        getDialog().setTitle(mContext.getResources().getString(R.string.info_weather));

        loadWeatherData();
        return v;
    }





    public void loadWeatherData(){
        gpsTracker= new GPSTracker((Activity) mContext);
        Location location = gpsTracker.getLocation();
        String mLoc=getAddressFromLocation(location);
        tvLocationName.setText(mLoc);
        Calendar c= Calendar.getInstance();
        tvDate.setText((c.get(Calendar.DAY_OF_WEEK))+"-"+(c.get(Calendar.MONTH)+1)+"-"+c.get(Calendar.YEAR));

        String URL= "http://api.openweathermap.org/data/2.5/weather?lat="+location.getLatitude()+"&lon="+location.getLongitude()+"&units=metric&appid=a361b8155883788d056585b0af36b57e";

        Log.e("URL: ",URL);
        final RequestQueue rq = Volley.newRequestQueue(mContext);
        final JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, URL, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("Weather: ",response.toString());
                try {
                    Weather weather= JSONWeatherParser.getWeather(response);
                    Log.e("INFO: ","city: "+weather.location.getCity()
                    +"---"+weather.location.getSunset()
                    +"---"+weather.temperature.getTemp());
                    String link="http://openweathermap.org/img/w/"+weather.currentCondition.getIcon()+".png";
                    Log.e("Link: ",link);
                    int temp =Math.round(weather.temperature.getTemp());
                    int code=weather.currentCondition.getWeatherId();
                    Picasso.with(mContext).load(R.drawable.rain_night).into(imgWeather);
                    //String weather_des = "";
//                    if (weather.currentCondition.getCondition().equals("Rain")){
//                        weather_des= utilities.convertRainCode(code);
//                    }

                    float weather_speech=weather.wind.getSpeed();
                    String weather_dir=utilities.convertDegreeToDirection(weather.wind.getDeg());
                    float weatheraa_humi=weather.currentCondition.getHumidity();

                    tvWeatherTemp.setText(temp+"");
                    tvWeatherDes.setText(weather.currentCondition.getCondition());
                    tvWeatherHumi.setText(AppConstance.WEATHER_HUMI+weatheraa_humi+" %");
                    tvWeatherSpeech.setText(AppConstance.WEATHER_SPEECH+weather_speech+" m/s");
                    tvWeatherDir.setText(AppConstance.WEATHER_DIR+weather_dir);
                    int drawable=utilities.imageDrawable(weather.currentCondition.getIcon());
                    Picasso.with(mContext).load(drawable).into(imgWeather);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


            }
        });

        rq.add(request);
    }

    public void loadImageData(String url){
        ImageRequest imageRequest = new ImageRequest(url, new Response.Listener<Bitmap>() {
            @Override
            public void onResponse(Bitmap response) {
                Log.e("Respone: ",response.toString());
                imgWeather.setImageBitmap(response);
            }
        },0,0,null,null);
    }


    private String getAddressFromLocation(Location location) {
        try {
            List<Address> list =
                    mGeocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            String locationName = list.get(0).getAddressLine(0);
            return locationName;
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }

}
