package chung.com;

import android.content.Context;
import android.graphics.Color;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;
import org.w3c.dom.Document;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import chung.com.adapter.InfoAdapter;
import chung.com.googlevoice.ActivityMap;
import chung.com.googlevoice.R;
import chung.com.model.Direction;
import chung.com.model.Place;
import chung.com.parser.PlaceJSONParser;

/**
 * Created by Admin on 4/04/2016.
 */
public class MapManager implements GoogleMap.OnMyLocationChangeListener, GoogleMap.OnMapClickListener, GoogleMap.OnMarkerClickListener {
    private GoogleMap mGoogleMap;
    public static Location mLocation;
    private Marker mMarker;
    private Geocoder geocoder;
    private String myLocationName;
    private Context context;
    private InfoAdapter infoAdapter;
    private Polyline polyline;
    private Marker markerStart;
    private Marker markerEnd;
    private String distance;
    private String duration;

    // A String array containing place types sent to Google Place service
    private  String[] mPlaceType = null;

    // Stores near by places
    public static Place[] mPlaces = null;
    // Links marker id and place object
    public static HashMap<String, Place> mHMReference = new HashMap<String, Place>();

    // Specifies the drawMarker() to draw the marker with default color
    private static final float UNDEFINED_COLOR = -1;

    public MapManager(GoogleMap mGoogleMap, Context context) {
        this.context = context;
        this.mGoogleMap = mGoogleMap;
        // Array of place types
        mPlaceType = context.getResources().getStringArray(R.array.place_type);
        geocoder = new Geocoder(context, Locale.getDefault());
        initMaps();
        infoAdapter = new InfoAdapter(context);
        mGoogleMap.setInfoWindowAdapter(infoAdapter);
        mGoogleMap.setOnMarkerClickListener(this);
    }

    private void initMaps() {
        mGoogleMap.getUiSettings().setZoomControlsEnabled(true);
        mGoogleMap.setMyLocationEnabled(true);
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mGoogleMap.setOnMapClickListener(this);
        mGoogleMap.setOnMyLocationChangeListener(this);
    }

    @Override
    public void onMyLocationChange(Location location) {
        mLocation = location;
        LatLng latLng = new LatLng(mLocation.getLatitude(), mLocation.getLongitude());
        if (mMarker == null) {
            myLocationName = getAddressName(mLocation);
            mMarker = drawMarker(mLocation, BitmapDescriptorFactory.HUE_RED, "My location");
            CameraPosition cameraPosition = new CameraPosition(latLng, 17, 0, 0);
            mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        } else {
            mMarker.setPosition(latLng);
            mMarker.setSnippet(myLocationName);

        }
    }

    public Marker drawMarker(Location location, float hue, String title) {
        MarkerOptions options = new MarkerOptions();
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        options.position(latLng);
        options.icon(BitmapDescriptorFactory.defaultMarker(hue));
        options.title(title);
        return mGoogleMap.addMarker(options);
    }

    public String getAddressName(Location location) {
        try {
            List<android.location.Address> list =
                    geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            String locationName = list.get(0).getAddressLine(0);
            locationName += " - " + list.get(0).getAddressLine(1);
            locationName += " - " + list.get(0).getAddressLine(2);
            return locationName;
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }

    public void search(Location location1, String diem2) {
        //Location location1 = getAddressLocation(diem1);
        Location location2 = getAddressLocation(diem2);
        if (location1 == null || location2 == null) {
            Toast.makeText(context, "Diem khong ton tai", Toast.LENGTH_LONG).show();
        } else {
            Log.e("Diem 1:", location1.getLatitude() + ":" + location1.getLongitude());
            Log.e("Diem 2:", location2.getLatitude() + ":" + location2.getLongitude());
            MyAsyctask myAsyctask = new MyAsyctask();
            myAsyctask.execute(location1, location2);
        }
    }

    public Location getAddressLocation(String name) {
        try {
            List<android.location.Address> list = geocoder.getFromLocationName(name, 1);
            double latitutde = list.get(0).getLatitude();
            double longitude = list.get(0).getLongitude();
            Location location = new Location("");
            location.setLatitude(latitutde);
            location.setLongitude(longitude);
            return location;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void onMapClick(LatLng latLng) {
        Log.e("Location: ",latLng.latitude + "---" + latLng.longitude);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        boolean check = ((ActivityMap)context).showFragmentDialogDetail(marker,mHMReference);
        return check;
    }

    class MyAsyctask extends AsyncTask<Location, Void, ArrayList<LatLng>> {

        @Override
        protected ArrayList<LatLng> doInBackground(Location... params) {
            Direction direction = new Direction();
            Document document = direction.getDocument(params[0], params[1]);
            ArrayList<LatLng> arrPoint = direction.getDirection(document);
            distance = direction.getDistanceText(document);
            duration = direction.getDurationText(document);
            return arrPoint;
        }

        @Override
        protected void onPostExecute(ArrayList<LatLng> latLngs) {
            super.onPostExecute(latLngs);
            PolylineOptions options = new PolylineOptions();
            options.width(5);
            options.color(Color.RED);
            options.addAll(latLngs);
            mGoogleMap.clear();
            if (polyline != null) {
                polyline.remove();
            }
            if (markerStart != null && markerEnd != null) {
                markerEnd.remove();
                markerStart.remove();
            }
            if (latLngs.size() >= 2) {
                Location locationStart = new Location("");
                locationStart.setLatitude(latLngs.get(0).latitude);
                locationStart.setLongitude(latLngs.get(0).longitude);
                String nameStart = getAddressName(locationStart);
                Location locationEnd = new Location("");
                locationEnd.setLatitude(latLngs.get(latLngs.size() - 1).latitude);
                locationEnd.setLongitude(latLngs.get(latLngs.size() - 1).longitude);
                String nameEnd = getAddressName(locationEnd);
                markerStart = drawMarker(locationStart, BitmapDescriptorFactory.HUE_ROSE, "Distance: " + distance + " - " + "Time: " + duration);
                markerEnd = drawMarker(locationEnd, BitmapDescriptorFactory.HUE_GREEN, "Distance: " + distance + " - " + "Time: " + duration);
                markerStart.setSnippet(nameStart);
                markerEnd.setSnippet(nameEnd);
            }
            polyline = mGoogleMap.addPolyline(options);
        }
    }

    public void showNearByPlace(int selectedPosition, Location location) {
        String type = mPlaceType[selectedPosition];

        mGoogleMap.clear();


        if (location == null) {
            Toast.makeText(context, "Please mark a location", Toast.LENGTH_SHORT).show();
            return;
        }

        drawMarker(location, BitmapDescriptorFactory.HUE_ROSE, "");

        StringBuilder sb = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        sb.append("location=" + location.getLatitude() + "," + location.getLongitude());
        sb.append("&radius=3000");
        sb.append("&types=" + type);
        sb.append("&sensor=true");
        sb.append("&key=AIzaSyBjZ2DyzYomkURLLoO3akCq5BZHjHKo7tA");

        Log.e("Map Manager: ",sb.toString());


        // Creating a new non-ui thread task to download Google place json data
        PlacesTask placesTask = new PlacesTask();

        // Invokes the "doInBackground()" method of the class PlaceTask
        placesTask.execute(sb.toString());
    }


    /**
     * A method to download json data from argument url
     */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);


            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Exception--->", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }


    /**
     * A class, to download Google Places
     */
    private class PlacesTask extends AsyncTask<String, Integer, String> {

        String data = null;

        // Invoked by execute() method of this object
        @Override
        protected String doInBackground(String... url) {
            try {
                Log.e("URL:", url[0]);
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        // Executed after the complete execution of doInBackground() method
        @Override
        protected void onPostExecute(String result) {
            ParserTask parserTask = new ParserTask();

            // Start parsing the Google places in JSON format
            // Invokes the "doInBackground()" method of ParserTask
            parserTask.execute(result);
        }

    }

    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, Place[]> {

        JSONObject jObject;

        // Invoked by execute() method of this object
        @Override
        protected Place[] doInBackground(String... jsonData) {


            Place[] places = null;
            PlaceJSONParser placeJsonParser = new PlaceJSONParser();

            try {
                jObject = new JSONObject(jsonData[0]);
                /** Getting the parsed data as a List construct */
                places = placeJsonParser.parse(jObject);

            } catch (Exception e) {
                Log.d("Exception", e.toString());
            }
            return places;
        }

        // Executed after the complete execution of doInBackground() method
        @Override
        protected void onPostExecute(Place[] places) {

            mPlaces = places;

            for (int i = 0; i < places.length; i++) {

                Place place = places[i];

                // Getting latitude of the place
                double lat = Double.parseDouble(place.mLat);

                // Getting longitude of the place
                double lng = Double.parseDouble(place.mLng);

                LatLng latLng = new LatLng(lat, lng);
                Location location = new Location("");
                location.setLatitude(latLng.latitude);
                location.setLongitude(latLng.longitude);

                Marker m = drawMarker(location, BitmapDescriptorFactory.HUE_GREEN, "");

                // Adding place reference to HashMap with marker id as HashMap key
                // to get its reference in infowindow click event listener
                mHMReference.put(m.getId(), place);

            }


        }

    }

}

