package chung.com.parser;


import android.os.Parcel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import chung.com.model.Clouds;
import chung.com.model.CurrentCondition;
import chung.com.model.Location;
import chung.com.model.Temperature;
import chung.com.model.Weather;
import chung.com.model.Wind;

/**
 * Created by root on 8/2/16.
 */
public class JSONWeatherParser {
    public static Weather getWeather(JSONObject jObj) throws JSONException  {

        Weather weather = new Weather();

        // We create out JSONObject from the data
        //JSONObject jObj = new JSONObject(data);git

        // We start extracting the info
        Location loc = new Location();
        CurrentCondition currentCondition = new CurrentCondition();
        Temperature temperature= new Temperature();
        Wind wind= new Wind();
        Clouds clouds= new Clouds();

        JSONObject coordObj = getObject("coord", jObj);
        loc.setLatitude(getFloat("lat", coordObj));
        loc.setLongitude(getFloat("lon", coordObj));

        JSONObject sysObj = getObject("sys", jObj);
        loc.setCountry(getString("country", sysObj));
        loc.setSunrise(getInt("sunrise", sysObj));
        loc.setSunset(getInt("sunset", sysObj));
        loc.setCity(getString("name", jObj));
        weather.location = loc;

        // We get weather info (This is an array)
        JSONArray jArr = jObj.getJSONArray("weather");

        // We use only the first value
        JSONObject JSONWeather = jArr.getJSONObject(0);
        currentCondition.setWeatherId(getInt("id", JSONWeather));
        currentCondition.setDescr(getString("description", JSONWeather));
        currentCondition.setCondition(getString("main", JSONWeather));
        currentCondition.setIcon(getString("icon", JSONWeather));

        JSONObject mainObj = getObject("main", jObj);
        currentCondition.setHumidity(getInt("humidity", mainObj));
        currentCondition.setPressure(getInt("pressure", mainObj));
        weather.currentCondition=currentCondition;

        temperature.setMaxTemp(getFloat("temp_max", mainObj));
        temperature.setMinTemp(getFloat("temp_min", mainObj));
        temperature.setTemp(getFloat("temp", mainObj));
        weather.temperature=temperature;

        // Wind
        JSONObject wObj = getObject("wind", jObj);
        wind.setSpeed(getFloat("speed", wObj));
        wind.setDeg(getFloat("deg", wObj));
        weather.wind=wind;

        // Clouds
        JSONObject cObj = getObject("clouds", jObj);
        clouds.setPerc(getInt("all", cObj));
        weather.clouds=clouds;

        // We download the icon to show


        return weather;
    }

    public static ArrayList<Weather> getListWeather(JSONObject jsonObject) throws JSONException{
        ArrayList<Weather> arrWeather= new ArrayList<>();
        JSONArray weatherArray= jsonObject.getJSONArray("list");
        for (int i =0 ; i< weatherArray.length(); i++){
            Weather weather= new Weather();
            CurrentCondition currentCondition= new CurrentCondition();
            Temperature temperature= new Temperature();
            Wind wind= new Wind();

           // Location loc = new Location();

            JSONArray jArr= weatherArray.getJSONObject(i).getJSONArray("weather");
            JSONObject JSONWeather=jArr.getJSONObject(0);
            currentCondition.setWeatherId(getInt("id", JSONWeather));
            currentCondition.setDescr(getString("description", JSONWeather));
            currentCondition.setCondition(getString("main", JSONWeather));
            currentCondition.setIcon(getString("icon", JSONWeather));

            JSONObject mainObj=weatherArray.getJSONObject(i).getJSONObject("main");
            currentCondition.setHumidity(getInt("humidity", mainObj));
            currentCondition.setPressure(getInt("pressure", mainObj));

            temperature.setMaxTemp(getFloat("temp_max", mainObj));
            temperature.setMinTemp(getFloat("temp_min", mainObj));
            temperature.setTemp(getFloat("temp", mainObj));

            JSONObject wObj=weatherArray.getJSONObject(i).getJSONObject("wind");
            wind.setSpeed(getFloat("speed", wObj));
            wind.setDeg(getFloat("deg", wObj));

            currentCondition.setDate(weatherArray.getJSONObject(i).getString("dt_txt"));
            weather.currentCondition=currentCondition;
            weather.temperature=temperature;
            weather.wind=wind;

            arrWeather.add(weather);
        }
        return arrWeather;
    }


    private static JSONObject getObject(String tagName, JSONObject jObj)  throws JSONException {
        JSONObject subObj = jObj.getJSONObject(tagName);
        return subObj;
    }

    private static String getString(String tagName, JSONObject jObj) throws JSONException {
        return jObj.getString(tagName);
    }

    private static float  getFloat(String tagName, JSONObject jObj) throws JSONException {
        return (float) jObj.getDouble(tagName);
    }

    private static int  getInt(String tagName, JSONObject jObj) throws JSONException {
        return jObj.getInt(tagName);
    }
}
