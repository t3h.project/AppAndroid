package chung.com.parser;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;

import chung.com.googlevoice.ActivitySearch;
import chung.com.model.ItemSearch;

/**
 * Created by Admin on 5/05/2016.
 */
public class ParserHtml extends AsyncTask<String, Void, ArrayList<ItemSearch>> {

    private static final String TAG = "ParserHtml";
    private ArrayList<ItemSearch> arr = new ArrayList<>();

    private Handler handler;

    public ParserHtml(Handler handler) {
        this.handler = handler;
    }


    @Override
    protected ArrayList<ItemSearch> doInBackground(String... strings) {
        String link = "https://www.bing.com/search?q="+strings[0];
        try {
            Document document = Jsoup.connect(link).userAgent("Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)").get();
            Elements elements = document.
                    select("ol#b_results").select("li.b_algo");

            // Elements elementTr = elements.select("div[class=g]");
            for (int i = 0; i < elements.size(); i++) {
                Elements elementT = elements.get(i).select("a");
                String title=elementT.text();
                Elements elementL = elements.get(i).select("cite");
                String mLink = elementL.text();
                Elements elementN = elements.get(i).select("p");
                String des = elementN.text();

                if (!mLink.contains("www.") && (!mLink.contains("https://")||mLink.contains("http://"))){
                        mLink="www."+mLink;
                }

                if (!mLink.contains("www.")&&(mLink.contains("https://")||mLink.contains("http://"))){
                    String l1=mLink.substring(mLink.indexOf("//")+2,mLink.length());
                    mLink="www."+l1;
                }

                ItemSearch club = new ItemSearch(title,mLink,des);
                arr.add(club);
                Log.e(TAG,title+" --- "+mLink+" --- "+des);
            }
            Log.e(TAG, elements.size() + "");

        } catch (IOException e) {
            e.printStackTrace();
        }
        return arr;
    }

    @Override
    protected void onPostExecute(ArrayList<ItemSearch> item) {
        super.onPostExecute(item);
        Message message=new Message();
        message.what= ActivitySearch.WHAT_PARSER;
        message.obj=item;
        handler.sendMessage(message);

    }
}
