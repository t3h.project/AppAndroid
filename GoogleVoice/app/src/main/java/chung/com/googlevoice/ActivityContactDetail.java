package chung.com.googlevoice;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import chung.com.common.AppConstance;
import chung.com.data.Manager;
import chung.com.model.Call;
import chung.com.model.Contact;

/**
 * Created by chung on 8/2/16.
 */
public class ActivityContactDetail extends AppCompatActivity implements View.OnClickListener {
    private Toolbar toolbar;
    private TextView tv_contact_detail_phone;
    private RelativeLayout rl_see_more, rl_call;
    private ImageView rl_mes;
    private Contact contact;
    private Manager manager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_detail);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            contact = (Contact) bundle.getSerializable(AppConstance.KEYDATA);
            getSupportActionBar().setTitle(contact.getName());
        }
        initView();
    }

    private void initView() {
        manager = new Manager(this);
        tv_contact_detail_phone = (TextView) findViewById(R.id.tv_contact_detail_phone);
        rl_call = (RelativeLayout) findViewById(R.id.rl_call);
        rl_see_more = (RelativeLayout) findViewById(R.id.rl_see_more);
        rl_mes = (ImageView) findViewById(R.id.rl_mes);
        tv_contact_detail_phone.setText(contact.getPhoneNo());
        rl_call.setOnClickListener(this);
        rl_mes.setOnClickListener(this);
        rl_see_more.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_call:
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + contact.getPhoneNo()));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ContextCompat.checkSelfPermission(ActivityContactDetail.this, android.Manifest.permission.CALL_PHONE)
                            != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(ActivityContactDetail.this,
                                new String[]{android.Manifest.permission.CALL_PHONE, android.Manifest.permission.CALL_PHONE},
                                1);
                    } else {
                        startActivity(intent);
                    }
                } else {
                    startActivity(intent);
                }
                break;
            case R.id.rl_mes:
                Intent intent1 = new Intent(this, Activity_Message.class);
                intent1.putExtra(AppConstance.KEY_CONTACT_DETAIL, contact);
                startActivity(intent1);
                finish();
                break;
            case R.id.rl_see_more:

                Intent intent2 = new Intent(this,Activity_History.class);
                intent2.putExtra(AppConstance.KEY_PHONE,contact.getPhoneNo());
                startActivity(intent2);

                for (Call call : manager.getHistoryCall(contact.getPhoneNo())
                        ) {
                    Log.e("callog", call.toString());
                }

break;


        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
