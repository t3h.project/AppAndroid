package chung.com.googlevoice;

import android.Manifest;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.Locale;

import chung.com.adapter.RVMessage;
import chung.com.common.AppConstance;
import chung.com.data.Manager;
import chung.com.model.Contact;
import chung.com.model.Sms;


/**
 * Created by chung on 6/5/16.
 */
public class Activity_Message extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "Activity_Message";
    private String code;
    private EditText edtContact_ms, edtContent_ms;
    private ImageView imContact_ms, imSend_ms, im_ms_voice;
    private Manager manager;
    private ArrayList<Sms> list;
    private RecyclerView RVMes;
    private RVMessage adapter;
    private Contact contact = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        initView();
        Bundle bundle = getIntent().getExtras();
        if (bundle !=null){
            contact = (Contact) bundle.getSerializable(AppConstance.KEY_CONTACT_DETAIL);
            edtContact_ms.setText(contact.toString());
            getPermissionReadMessage();
            adapter.notifyDataSetChanged();
        }
    }

    private void initView() {
        manager = new Manager(this);
        list = new ArrayList<>();
        RVMes = (RecyclerView) findViewById(R.id.RVMes);
        edtContact_ms = (EditText) findViewById(R.id.edtContact_ms);
        edtContent_ms = (EditText) findViewById(R.id.edtContent_ms);
        imContact_ms = (ImageView) findViewById(R.id.imContact_ms);
        imSend_ms = (ImageView) findViewById(R.id.imSend_ms);
        im_ms_voice = (ImageView) findViewById(R.id.im_ms_voice);
        im_ms_voice.setOnClickListener(this);
        imSend_ms.setOnClickListener(this);
        imContact_ms.setOnClickListener(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setReverseLayout(true);
        RVMes.setLayoutManager(linearLayoutManager);

        adapter = new RVMessage(this, list);
        RVMes.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        code = "";
    }

    private void setSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Đang nghe");
        try {
            startActivityForResult(intent, AppConstance.REQUEST_CODE_MESSAGE);
        } catch (ActivityNotFoundException a) {
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AppConstance.REQUEST_CODE_MESSAGE && resultCode == this.RESULT_OK && data != null) {
            ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            String textResult = result.get(0);
            edtContent_ms.setText(edtContent_ms.getText().toString() + " " + textResult);
            edtContent_ms.setSelection(edtContent_ms.getText().length());
        } else if (requestCode == AppConstance.KEY_RESULT) {
            if (resultCode == Activity.RESULT_OK) {
                contact = new Contact();
                contact = (Contact) data.getSerializableExtra(AppConstance.KEY_CONTACT);
                edtContact_ms.setText(contact.toString());
                getPermissionReadMessage();
                adapter.notifyDataSetChanged();
            }
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imContact_ms:
                list.clear();
                code = "message";
                Intent intent = new Intent(Activity_Message.this, ActivityContact.class);
                intent.putExtra(AppConstance.KEY_MES, code);
                startActivityForResult(intent, AppConstance.KEY_RESULT);
                break;
            case R.id.im_ms_voice:
                setSpeechInput();
                break;
            case R.id.imSend_ms:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                    if (ContextCompat.checkSelfPermission(Activity_Message.this, Manifest.permission.SEND_SMS)
                            != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(Activity_Message.this,
                                new String[]{android.Manifest.permission.SEND_SMS, android.Manifest.permission.SEND_SMS},
                                1);
                    } else {
                        sendMes();
                    }
                else {
                    sendMes();
                }

                break;
        }
    }

    public void sendMes() {
        String mes = null;
        String phone = edtContact_ms.getText().toString();
        String body = edtContent_ms.getText().toString();

        if (body.equals("") || phone.equals("")) {
            mes = "Bạn chưa nhập đủ thông tin ";
        } else {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(contact.getPhoneNo(), null, edtContent_ms.getText().toString(), null, null);
            mes = "Đã gửi tin nhắn tới số " + contact.getPhoneNo();
        }
        showDialog(mes);

    }

    public void update() {
        edtContent_ms.setText("");
        list.clear();
        list.addAll(manager.getAllSms(contact.getPhoneNo()));
        adapter.notifyDataSetChanged();
    }

    public void showDialog(String mes) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage(mes);
        builder1.setCancelable(true);
        builder1.setPositiveButton(
                "Đóng",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        update();
                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }


    public void getPermissionReadMessage() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            if (ContextCompat.checkSelfPermission(Activity_Message.this, Manifest.permission.READ_SMS)
                    != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(Activity_Message.this,
                        new String[]{android.Manifest.permission.READ_SMS, android.Manifest.permission.READ_SMS},
                        1);
            } else {
                list.clear();
                list.addAll(manager.getAllSms(contact.getPhoneNo()));
            }
        else {
            list.clear();
            list.addAll(manager.getAllSms(contact.getPhoneNo()));
        }
        Log.e("size", list.size() + "");
        for (Sms s : list
                ) {
            Log.e("sms", s.toString());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        adapter.notifyDataSetChanged();
    }
}
