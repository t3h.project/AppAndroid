package chung.com.googlevoice;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.speech.RecognizerIntent;
import android.support.annotation.Nullable;
import android.support.customtabs.CustomTabsClient;
import android.support.customtabs.CustomTabsIntent;
import android.support.customtabs.CustomTabsServiceConnection;
import android.support.customtabs.CustomTabsSession;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Locale;

import chung.com.adapter.ItemSearchAdapter;
import chung.com.common.AppConstance;
import chung.com.model.ItemSearch;
import chung.com.onclick.IOnItemClickListener;
import chung.com.parser.ParserHtml;

/**
 * Created by root on 7/20/16.
 */
public class ActivitySearch extends AppCompatActivity implements IOnItemClickListener {

    public final String CUSTOM_TAB_PACKAGE_NAME = "com.android.chrome";
    public static final int WHAT_PARSER = 1;
    private EditText mTvSearch;
    private ArrayList<ItemSearch> arrItem = new ArrayList<>();
    private ItemSearchAdapter adapter;
    private RecyclerView recyclerView;
    private FrameLayout frLayout;
    private String link = "";
    private CustomTabsClient mCustomTabsClient;
    private CustomTabsSession mCustomTabsSession;
    private CustomTabsServiceConnection mCustomTabsServiceConnection;
    private CustomTabsIntent mCustomTabsIntent;

    private static final String TAG = "activity_search";
    private ImageView ivVoice;

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        initView();
        setupCustomTabs();

    }

    private void setupCustomTabs(){
        mCustomTabsServiceConnection = new CustomTabsServiceConnection() {
            @Override
            public void onCustomTabsServiceConnected(ComponentName componentName, CustomTabsClient customTabsClient) {
                mCustomTabsClient= customTabsClient;
                mCustomTabsClient.warmup(0L);
                mCustomTabsSession = mCustomTabsClient.newSession(null);
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                mCustomTabsClient= null;
            }
        };

        CustomTabsClient.bindCustomTabsService(this, CUSTOM_TAB_PACKAGE_NAME, mCustomTabsServiceConnection);

        mCustomTabsIntent = new CustomTabsIntent.Builder(mCustomTabsSession)
                .setShowTitle(true)
                .setStartAnimations(this,R.anim.slide_in_right,R.anim.slide_out_left)
                .setExitAnimations(this, android.R.anim.slide_in_left,
                        android.R.anim.slide_out_right)
                .build();
    }

    public void startChromeCustomTab(String link) {
        String websiteURL="http://"+link+"/";
        Log.e(TAG,websiteURL);
        mCustomTabsIntent.launchUrl(this, Uri.parse(websiteURL));
    }


    private void initView() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_USE_LOGO | ActionBar.DISPLAY_SHOW_HOME
                | ActionBar.DISPLAY_HOME_AS_UP);

        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewSearch = inflater.inflate(R.layout.layout_search, null);
        mTvSearch = (EditText) viewSearch.findViewById(R.id.tv_search);
        mTvSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_SEARCH) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                    parser(mTvSearch.getText().toString());
                    return true;
                }
                return false;
            }
        });

        frLayout = (FrameLayout) findViewById(R.id.frame_no_data);
        //init voice
        ivVoice = (ImageView) viewSearch.findViewById(R.id.iv_voice);
        getSupportActionBar().setCustomView(viewSearch);
        ivVoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent speechRecognitionIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                speechRecognitionIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault().toString());
                speechRecognitionIntent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                        getString(R.string.talking));
                startActivityForResult(speechRecognitionIntent, AppConstance.SPEECHINTENT_REQ_CODE);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == AppConstance.SPEECHINTENT_REQ_CODE && resultCode == RESULT_OK) {
            ArrayList<String> speechResults = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            String text = speechResults.get(0);
            mTvSearch.setText(text);
            //Check(text);
            parser(text);
        }
    }

    private void parser(String text){
        try {
            String result= URLEncoder.encode(text,"utf-8");
            ParserHtml parserHtml = new ParserHtml(handler);
            parserHtml.execute(result);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }


////
//    private void Check(String text) {
//        if (text.toLowerCase().contains("ok google")) {
//
//        } else if (text != null) {
//           // mWebView.loadUrl(LINK + mTvSearch.getText().toString());
//        }
//    }
////



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == WHAT_PARSER) {
                arrItem.clear();
                arrItem.addAll((ArrayList<ItemSearch>) msg.obj);
                recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
                RecyclerView.LayoutManager manager = new LinearLayoutManager(ActivitySearch.this);
                recyclerView.setLayoutManager(manager);
                adapter = new ItemSearchAdapter(ActivitySearch.this, arrItem);
                recyclerView.setAdapter(adapter);
                adapter.setmOnItemClickListener(ActivitySearch.this);
                adapter.notifyDataSetChanged();
                frLayout.setVisibility(View.GONE);
            }
        }
    };


    @Override
    public void onClickItem(View v, int pos) {
        ItemSearch itemSearch = arrItem.get(pos);
        Log.e("limk: ", itemSearch.getLink());
        startChromeCustomTab(itemSearch.getLink());
    }
}
