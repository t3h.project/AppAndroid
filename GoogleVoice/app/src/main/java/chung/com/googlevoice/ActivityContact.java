package chung.com.googlevoice;

import android.Manifest;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.ArrayList;
import java.util.Locale;

import chung.com.adapter.RVContact;
import chung.com.callback.ItemClick;
import chung.com.common.AppConstance;
import chung.com.data.Manager;
import chung.com.model.Contact;

/**
 * Created by chung on 7/27/16.
 */
public class ActivityContact extends AppCompatActivity implements View.OnClickListener, ItemClick {


    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private RVContact adapter;
    private EditText edtSearch_Contact;
    private ToggleButton ivVoice_Contact;
    private Manager manager;
    private ArrayList<Contact> list;
    private TextView tvContactName1, tvContactPhone1;
    private LinearLayout mLinearSearch;
    private Contact contact;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
        initView();
    }

    private void initView() {
        manager = new Manager(this);
        list = new ArrayList<>();
        edtSearch_Contact = (EditText) findViewById(R.id.edtSearch_Contact);
        ivVoice_Contact = (ToggleButton) findViewById(R.id.ivVoice_contact);
        ivVoice_Contact.setOnClickListener(this);
        mRecyclerView = (RecyclerView) findViewById(R.id.mRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
        adapter = new RVContact(this, list);
        mRecyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        tvContactName1 = (TextView) findViewById(R.id.tvContactName1);
        tvContactPhone1 = (TextView) findViewById(R.id.tvContactPhone1);
        mLinearSearch = (LinearLayout) findViewById(R.id.mLinearSearch);
        mLinearSearch.setVisibility(View.GONE);
        getPermissionRead();

        setSpeechInput();
        mLinearSearch.setOnClickListener(this);
    }

    public void getPermissionRead() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            if (ContextCompat.checkSelfPermission(ActivityContact.this, Manifest.permission.READ_CONTACTS)
                    != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(ActivityContact.this,
                        new String[]{android.Manifest.permission.READ_CONTACTS, android.Manifest.permission.READ_CONTACTS},
                        1);
            } else {
                list.addAll(manager.getContact());
            }
        else {
            list.addAll(manager.getContact());
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivVoice_contact:
                setSpeechInput();
                break;
            case R.id.mLinearSearch:
                Bundle bundle = getIntent().getExtras();
                if (bundle != null) {
                    contact = new Contact(tvContactName1.getText().toString(), tvContactPhone1.getText().toString());
                    Intent returnIntent = new Intent();
                    Log.e("contact", contact.toString());
                    returnIntent.putExtra(AppConstance.KEY_CONTACT, contact);
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                } else {
                    Intent intent = new Intent(this,ActivityContactDetail.class);
                    intent.putExtra(AppConstance.KEYDATA,contact);
                    startActivity(intent);
                }
                break;
        }
    }

    private void setSpeechInput() {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Đang nghe");
        try {
            startActivityForResult(intent, AppConstance.REQUEST_CODE_CONTACT);
        } catch (ActivityNotFoundException a) {
            Toast.makeText(this, a.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AppConstance.REQUEST_CODE_CONTACT && resultCode == this.RESULT_OK && data != null) {
            ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            String txtSearch = result.get(0);
            edtSearch_Contact.setText(txtSearch);
            for (Contact contact : list) {
                if (txtSearch.toLowerCase().contains("gọi") && txtSearch.toLowerCase().contains(contact.getName().toLowerCase())) {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + contact.getPhoneNo()));
                    if (ContextCompat.checkSelfPermission(ActivityContact.this, Manifest.permission.CALL_PHONE)
                            != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(ActivityContact.this,
                                new String[]{android.Manifest.permission.CALL_PHONE, android.Manifest.permission.CALL_PHONE},
                                1);
                    } else {
                        startActivity(intent);
                    }
                }
                if (txtSearch.toLowerCase().contains(contact.getName().toLowerCase())) {
                    mLinearSearch.setVisibility(View.VISIBLE);
                    tvContactName1.setText(contact.getName());
                    tvContactPhone1.setText(contact.getPhoneNo());
                }
            }

        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void selectedItem(Object obj, String type) {
        if (obj instanceof Contact) {
            Contact contact = (Contact) obj;
            switch (type) {
                case AppConstance.INTERFACE_CLICK_JOB:
                    Bundle bundle = getIntent().getExtras();
                    if (bundle != null) {
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra(AppConstance.KEY_CONTACT, contact);
                        Log.e("contact", contact.toString());
                        setResult(Activity.RESULT_OK, returnIntent);
                        finish();
                    }else {
                        Intent intent = new Intent(this,ActivityContactDetail.class);
                        intent.putExtra(AppConstance.KEYDATA,contact);
                        startActivity(intent);
                    }
                    break;
            }
        }
    }
}
