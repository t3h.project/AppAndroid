package chung.com.googlevoice;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.speech.RecognizerIntent;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.Locale;

import chung.com.GPSTracker;
import chung.com.VNCharacterUtils;
import chung.com.common.AppConstance;
import chung.com.data.Manager;
import chung.com.dialog.WeatherDialogFragment;
import chung.com.model.Song;
import chung.com.utilities.Utilities;


/**
 * Created by root on 7/18/16.
 */
public class ActivityStart extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "ActivityStart";
    private TextView tvResult;
    private WifiManager wifiManager;
    private BluetoothAdapter bluetoothAdapter;
    private ArrayList<Song> listSong;
    private Manager manager;
    private ImageView imgVoice;
    private GPSTracker gpsTracker;

    private Boolean permissionRequestInProgress = false;
    private Utilities utilities;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        manager = new Manager(this);
        utilities = new Utilities(this);
        boolean ok = utilities.isGPS();
        Toast.makeText(this, "Turn on GPS: " + ok, Toast.LENGTH_LONG).show();
        initView();
        if (utilities.isConnected()) {
            startSpeechRecognizer();
        } else {
            utilities.showSettingsAlert();
        }


    }

    private void initView() {
        listSong = new ArrayList<>();
        getPermissionRead();
        tvResult = (TextView) findViewById(R.id.tvResult);
        imgVoice = (ImageView) findViewById(R.id.toggleButton1);
        imgVoice.setOnClickListener(this);
    }

    public void getPermissionRead() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            if (ContextCompat.checkSelfPermission(ActivityStart.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(ActivityStart.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                        1);
            } else {
                listSong.addAll(manager.getSong());
            }
        else {
            listSong.addAll(manager.getSong());
        }
    }

    private void startSpeechRecognizer() {
        Intent speechRecognitionIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        speechRecognitionIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault().toString());
        speechRecognitionIntent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                getString(R.string.talking));
        startActivityForResult(speechRecognitionIntent, AppConstance.SPEECHINTENT_REQ_CODE);
    }


    public void changeBrightness(int t) {
        try {
            int br = Settings.System.getInt(getContentResolver(), Settings.System.SCREEN_BRIGHTNESS);
            WindowManager.LayoutParams lp = getWindow().getAttributes();
            lp.screenBrightness = (float) br / t;
            getWindow().setAttributes(lp);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void Check(String txtResult) {
        if (txtResult.toLowerCase().contains("tìm kiếm")) {
            startActivity(new Intent(this, ActivitySearch.class));
        } else if (txtResult.toLowerCase().contains("bật wifi")) {
            setWifi(true);
        } else if (txtResult.toLowerCase().contains("tắt wifi")) {
            setWifi(false);
        } else if (txtResult.toLowerCase().contains("bật bluetooth")) {
            setBluetooth(true);
        } else if (txtResult.toLowerCase().contains("tắt bluetooth")) {
            setBluetooth(false);
        } else if (txtResult.toLowerCase().contains("gọi điện")) {
            startActivity(new Intent(this, ActivityContact.class));
        } else if (txtResult.toLowerCase().contains("tìm đường")) {
            startActivity(new Intent(this, ActivityMap.class));
        } else if (txtResult.toLowerCase().contains("nhắn tin")) {
            startActivity(new Intent(this, Activity_Message.class));

        }else if (txtResult.toLowerCase().contains("thời tiết trong tuần")) {
            if (utilities.isGPS()) {
                startActivity(new Intent(this, ActivityWeather.class));
            } else {
                utilities.showSettingsGPSAlert();
            }

        } else if (txtResult.toLowerCase().contains("thời tiết")) {
            if (utilities.isGPS()) {
                showFragmentDialogDetail();
            } else {
                utilities.showSettingsGPSAlert();
            }


        }

//            PlacePicker.IntentBuilder builder= new PlacePicker.IntentBuilder();
//
//            try {
//                Intent intent= builder.build(getApplicationContext());
//                startActivityForResult(intent,PLACE_PICKER_REQUEST);
//            } catch (GooglePlayServicesRepairableException e) {
//                e.printStackTrace();
//            } catch (GooglePlayServicesNotAvailableException e) {
//                e.printStackTrace();
//            }

        for (Song song : listSong) {
            if (VNCharacterUtils.removeAccent(txtResult.toUpperCase())
                    .equals(VNCharacterUtils.removeAccent(song.getTitle().toUpperCase()))) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                File file = new File(song.getData());
                intent.setDataAndType(Uri.fromFile(file), "audio/*");
                startActivity(intent);
            }
        }
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == AppConstance.SPEECHINTENT_REQ_CODE && resultCode == RESULT_OK) {
            ArrayList<String> speechResults = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            String text = speechResults.get(0);
            tvResult.setText(text);
            Check(text);
        }
    }

    public void setWifi(Boolean enable) {
        wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        if (!wifiManager.isWifiEnabled() && enable) {
            wifiManager.setWifiEnabled(true);
        }
        if (wifiManager.isWifiEnabled() && !enable) {
            wifiManager.setWifiEnabled(false);
        }
    }

    public void setBluetooth(Boolean enable) {
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter.isEnabled() && !enable) {
            bluetoothAdapter.disable();
        }
        if (!bluetoothAdapter.isEnabled() && enable) {
            bluetoothAdapter.enable();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onClick(View view) {
        if (utilities.isConnected()) {
            startSpeechRecognizer();
        } else {
            utilities.showSettingsAlert();
        }

    }

    public boolean showFragmentDialogDetail() {
        // If touched at User input location
        // Creating a dialog fragment to display the photo
        WeatherDialogFragment dialogFragment = WeatherDialogFragment.newInstance(this);

        // Getting a reference to Fragment Manager
        FragmentManager fm = getSupportFragmentManager();

        // Starting Fragment Transaction
        FragmentTransaction ft = fm.beginTransaction();

        // Adding the dialog fragment to the transaction
        ft.add(dialogFragment, "TAG");

        // Committing the fragment transaction
        ft.commitAllowingStateLoss();

        return false;
    }




}
