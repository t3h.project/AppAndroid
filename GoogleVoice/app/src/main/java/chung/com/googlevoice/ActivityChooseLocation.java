package chung.com.googlevoice;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import chung.com.common.AppConstance;
import chung.com.utilities.Utilities;

/**
 * Created by root on 7/28/16.
 */
public class ActivityChooseLocation extends AppCompatActivity {

    private Spinner sp_choose_location;
    private Button btnSearchLocation;
    private Utilities utilities;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_choose_location);
        utilities = new Utilities(this);
        String[] mPlaceTypeName = getResources().getStringArray(R.array.place_type_name);
        sp_choose_location = (Spinner) findViewById(R.id.spr_place_type);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, mPlaceTypeName);
        adapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
        sp_choose_location.setAdapter(adapter);

        btnSearchLocation = (Button) findViewById(R.id.btn_search_location);
        btnSearchLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int pos = sp_choose_location.getSelectedItemPosition();
                if (utilities.isConnected() && utilities.isGPS()) {
                    Intent intent = new Intent();
                    intent.putExtra(AppConstance.KEY_POSITION, pos);
                    setResult(RESULT_OK, intent);
                    finish();

                } else {
                    utilities.showSettingsGPSAlert();
                }

            }
        });

    }
}
