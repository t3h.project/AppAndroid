package chung.com.googlevoice;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import chung.com.GPSTracker;
import chung.com.adapter.AdapterWeatherHorizontal;
import chung.com.adapter.AdapterWeatherVertical;
import chung.com.common.AppConstance;
import chung.com.model.Weather;
import chung.com.parser.JSONWeatherParser;
import chung.com.utilities.Utilities;

/**
 * Created by root on 8/3/16.
 */
public class ActivityWeather extends AppCompatActivity {

    private RecyclerView rycWeatherHorizontal, rycWeatherVertical;
    private AdapterWeatherHorizontal adapterWeatherHorizontal;
    private AdapterWeatherVertical adapterWeatherVertical;
    private EditText mTvSearch;
    private ImageView ivVoice;
    private GPSTracker gpsTracker;
    private Utilities utilities;
    private Location location;
    private String URL;
    private Geocoder geocoder;
    private ProgressDialog progress;
    private TextView tvWeatherDesWeek, tvWeatherCelWeek, tvWeatherHumiWeek, tvWeatherSpeechWeek;
    private ImageView imgWeatherWeek;
    private RequestQueue rq;
    private FrameLayout frame_no_data_weather;
    private ArrayList<Weather> arrW, arrWDay, arrWWeek;
    private Weather mWeather;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_weather_week);
        utilities = new Utilities(this);
        geocoder = new Geocoder(this, Locale.getDefault());
        rq = Volley.newRequestQueue(this);
        gpsTracker = new GPSTracker(this);
        location = gpsTracker.getLocation();
        URL = "http://api.openweathermap.org/data/2.5/forecast?lat=" + location.getLatitude() + "&lon=" + location.getLongitude() + "&units=metric&appid=a361b8155883788d056585b0af36b57e";
        initViews();
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("weather")
                    && savedInstanceState.containsKey("arrW")
                    && savedInstanceState.containsKey("arrDay")
                    && savedInstanceState.containsKey("arrWeek")) {
                mWeather = savedInstanceState.getParcelable("weather");
                arrW= savedInstanceState.getParcelableArrayList("arrW");
                arrWDay=savedInstanceState.getParcelableArrayList("arrDay");
                arrWWeek= savedInstanceState.getParcelableArrayList("arrWeek");
                String link = "http://openweathermap.org/img/w/" + mWeather.currentCondition.getIcon() + ".png";
                initData(arrWDay,arrWWeek);
                initWeatherData(mWeather,link);

            }

        }else{
            showProgressBar();
            loadWeatherData(URL);
            loadWeatherDataToday(location);
            stopProgressBar();
        }

        //initData();


    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (mWeather != null) {
            outState.putParcelable("weather", mWeather);
        }
        if (arrW != null && arrWDay != null && arrWWeek != null) {
            outState.putParcelableArrayList("arrW", arrW);
            outState.putParcelableArrayList("arrDay", arrWDay);
            outState.putParcelableArrayList("arrWeek", arrWWeek);
            //outState
        }
        super.onSaveInstanceState(outState);
    }

    private void initData(ArrayList<Weather> listWeatherDay, ArrayList<Weather> listWeatherWeek) {

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rycWeatherHorizontal.setLayoutManager(linearLayoutManager);
        adapterWeatherHorizontal = new AdapterWeatherHorizontal(this, listWeatherDay);
        rycWeatherHorizontal.setAdapter(adapterWeatherHorizontal);

        LinearLayoutManager lLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rycWeatherVertical.setLayoutManager(lLayoutManager);
        adapterWeatherVertical = new AdapterWeatherVertical(this, listWeatherWeek);
        rycWeatherVertical.setAdapter(adapterWeatherVertical);
        //stopProgressBar();

    }

    private void initViews() {

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        tvWeatherDesWeek = (TextView) findViewById(R.id.tv_weather_des_week);
        tvWeatherCelWeek = (TextView) findViewById(R.id.tv_weather_temp_week);
        tvWeatherSpeechWeek = (TextView) findViewById(R.id.tv_weather_speech_week);
        tvWeatherHumiWeek = (TextView) findViewById(R.id.tv_weather_humi_week);
        imgWeatherWeek = (ImageView) findViewById(R.id.img_weather_detail_week);
        frame_no_data_weather = (FrameLayout) findViewById(R.id.frame_no_data_weather);

        rycWeatherHorizontal = (RecyclerView) findViewById(R.id.recycler_weather_detail_day);
        rycWeatherVertical = (RecyclerView) findViewById(R.id.recycler_weather_detail_week);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_USE_LOGO | ActionBar.DISPLAY_SHOW_HOME
                | ActionBar.DISPLAY_HOME_AS_UP);

        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewSearch = inflater.inflate(R.layout.layout_search, null);
        mTvSearch = (EditText) viewSearch.findViewById(R.id.tv_search);
        mTvSearch.setHint(R.string.input_weather_name_location);
        mTvSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_SEARCH) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                    //showProgressBar();
                    Location locSearch = getLocationFromAddress(mTvSearch.getText().toString());
                    if (locSearch!=null){
                        String link ="https://maps.googleapis.com/maps/api/place/nearbysearch/json?location="+locSearch.getLatitude()+","+locSearch.getLongitude()+"&sensor=true&key=AIzaSyBjZ2DyzYomkURLLoO3akCq5BZHjHKo7tA";

                        URL = "http://api.openweathermap.org/data/2.5/forecast?lat=" + locSearch.getLatitude() + "&lon=" + locSearch.getLongitude() + "&units=metric&appid=a361b8155883788d056585b0af36b57e";
                        Log.e("Link Weather: ",link);
                        CheckAddress(link, URL, locSearch);
                        stopProgressBar();
                        // stopProgressBar();
                        //loadWeatherData(URL);
                        return true;
                    }else{
                        frame_no_data_weather.setVisibility(View.VISIBLE);
                        Toast.makeText(ActivityWeather.this,R.string.not_found_location,Toast.LENGTH_LONG).show();
                    }

                }
                //stopProgressBar();
                return false;
            }
        });

        //frLayout = (FrameLayout) findViewById(R.id.frame_no_data);
        //init voice
        ivVoice = (ImageView) viewSearch.findViewById(R.id.iv_voice);
        getSupportActionBar().setCustomView(viewSearch);
        ivVoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent speechRecognitionIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                speechRecognitionIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault().toString());
                speechRecognitionIntent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                        getString(R.string.talking));
                startActivityForResult(speechRecognitionIntent, AppConstance.SPEECHINTENT_REQ_CODE);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == AppConstance.SPEECHINTENT_REQ_CODE && resultCode == RESULT_OK) {
            ArrayList<String> speechResults = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            String text = speechResults.get(0);
            mTvSearch.setText(text);
            //showProgressBar();
            Location mLoc = getLocationFromAddress(text);
            if (mLoc!=null){
                String link = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location="+mLoc.getLatitude()+","+mLoc.getLongitude()+"&sensor=true&key=AIzaSyBjZ2DyzYomkURLLoO3akCq5BZHjHKo7tA";

                URL = "http://api.openweathermap.org/data/2.5/forecast?lat=" + mLoc.getLatitude() + "&lon=" + mLoc.getLongitude() + "&units=metric&appid=a361b8155883788d056585b0af36b57e";
                Log.e("Link Weather: ",link);

                CheckAddress(link, URL,mLoc);
                stopProgressBar();
            }else{
                frame_no_data_weather.setVisibility(View.VISIBLE);
                Toast.makeText(ActivityWeather.this,R.string.not_found_location,Toast.LENGTH_LONG).show();
            }

            //stopProgressBar();
        }
    }

    private void initWeatherData(Weather weather, String link) {
        tvWeatherDesWeek.setText(weather.currentCondition.getCondition());
        tvWeatherCelWeek.setText(Math.round(weather.temperature.getTemp()) + "");
        tvWeatherHumiWeek.setText(AppConstance.WEATHER_HUMI + weather.currentCondition.getHumidity() + " %");
        tvWeatherSpeechWeek.setText(AppConstance.WEATHER_SPEECH + weather.wind.getSpeed() + " m/s");
        Picasso.with(this).load(link).into(imgWeatherWeek);
        //stopProgressBar();

    }

    public void loadWeatherData(String url) {

        final Calendar c = Calendar.getInstance();
        final String dAfter = c.get(Calendar.YEAR) + "-" + (c.get(Calendar.MONTH) + 1) + "-" + (c.get(Calendar.DAY_OF_MONTH) + 1);
        Log.e("URL: ", url);
        //final RequestQueue rq = Volley.newRequestQueue(this);
        final JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //Log.e("Weather: ", response.toString());

                Calendar cl = Calendar.getInstance();
                try {
                    arrW = JSONWeatherParser.getListWeather(response);
                    arrWDay = new ArrayList<>();
                    arrWWeek = new ArrayList<>();

                    for (int i = 0; i < arrW.size(); i++) {
                        String d1 = arrW.get(i).currentCondition.getDate();

                        if (utilities.compareToAfterDate(d1, dAfter)) {
                            String date = utilities.formatTime(d1);
                            arrW.get(i).currentCondition.setDate(date);
                            arrWDay.add(arrW.get(i));
                        } else {
                            String dCur = arrW.get(i).currentCondition.getDate();
                            String dWeek1 = cl.get(Calendar.YEAR) + "-" + (cl.get(Calendar.MONTH) + 1) + "-" + (cl.get(Calendar.DAY_OF_MONTH) + 1) + " 03:00:00";
                            String dWeek2 = cl.get(Calendar.YEAR) + "-" + (cl.get(Calendar.MONTH) + 1) + "-" + (cl.get(Calendar.DAY_OF_MONTH) + 2) + " 03:00:00";
                            String dWeek3 = cl.get(Calendar.YEAR) + "-" + (cl.get(Calendar.MONTH) + 1) + "-" + (cl.get(Calendar.DAY_OF_MONTH) + 3) + " 03:00:00";
                            String dWeek4 = cl.get(Calendar.YEAR) + "-" + (cl.get(Calendar.MONTH) + 1) + "-" + (cl.get(Calendar.DAY_OF_MONTH) + 4) + " 03:00:00";

                            if (utilities.compareToEqualTime(dCur, dWeek1)
                                    || utilities.compareToEqualTime(dCur, dWeek2)
                                    || utilities.compareToEqualTime(dCur, dWeek3)
                                    || utilities.compareToEqualTime(dCur, dWeek4)) {
                                String fDate = utilities.fomatDate(arrW.get(i).currentCondition.getDate());
                                arrW.get(i).currentCondition.setDate(fDate);
                                arrWWeek.add(arrW.get(i));
                            }
                        }
                    }

                    initData(arrWDay, arrWWeek);

//                    Log.e("Size: ", arrW.size() + "");
//                    for (int i = 0; i < arrW.size(); i++) {
//                        Log.e("INFO: ", "city: " + arrW.get(i).currentCondition.getCondition()
//                                + "---" + arrW.get(i).currentCondition.getHumidity()
//                                + "---" + arrW.get(i).currentCondition.getWeatherId()
//                                + "---" + arrW.get(i).currentCondition.getDate());
//                        String link = "http://openweathermap.org/img/w/" + arrW.get(i).currentCondition.getIcon() + ".png";
//                    }
//

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (ParseException e2) {
                    e2.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });

        rq.add(request);
        //stopProgressBar();
    }

    public void loadWeatherDataToday(Location location) {

        String URL = "http://api.openweathermap.org/data/2.5/weather?lat=" + location.getLatitude() + "&lon=" + location.getLongitude() + "&units=metric&appid=a361b8155883788d056585b0af36b57e";
        Log.e("URL: ", URL);
        final JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, URL, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //Log.e("Weather: ",response.toString());
                try {
                    mWeather = JSONWeatherParser.getWeather(response);
//                    Log.e("INFO: ","city: "+weather.location.getCity()
//                            +"---"+weather.location.getSunset()
//                            +"---"+weather.temperature.getTemp());
                    String link = "http://openweathermap.org/img/w/" + mWeather.currentCondition.getIcon() + ".png";
                    // Log.e("Link: ",link);

                    initWeatherData(mWeather, link);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });

        rq.add(request);
    }

    public Location getLocationFromAddress(String name) {
        try {
            List<Address> list = geocoder.getFromLocationName(name, 1);
            double latitutde = list.get(0).getLatitude();
            double longitude = list.get(0).getLongitude();
            Location location = new Location("");
            location.setLatitude(latitutde);
            location.setLongitude(longitude);
            return location;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void showProgressBar() {
        progress = ProgressDialog.show(ActivityWeather.this, "",
                getResources().getString(R.string.waiting_load_data), true);
    }

    private void stopProgressBar() {
        Runnable progressRunnable = new Runnable() {

            @Override
            public void run() {
                //progress.cancel();
                progress.dismiss();
            }
        };

        Handler pdCanceller = new Handler();
        pdCanceller.postDelayed(progressRunnable, 3000);
    }

    private boolean CheckAddress(String url, final String urlExcute,final  Location loc) {
        //final RequestQueue rq = Volley.newRequestQueue(this);
        final JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                showProgressBar();
                try {
                    String status = response.getString("status");
                    if (!status.equals("OK")) {
                        frame_no_data_weather.setVisibility(View.VISIBLE);
                        Toast.makeText(getApplicationContext(), R.string.not_found_location, Toast.LENGTH_LONG).show();
                    } else {
                        frame_no_data_weather.setVisibility(View.GONE);
                        loadWeatherData(urlExcute);
                        loadWeatherDataToday(loc);
                    }
                    //stopProgressBar();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


            }
        });

        rq.add(request);
        return false;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();
        if (id==android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
