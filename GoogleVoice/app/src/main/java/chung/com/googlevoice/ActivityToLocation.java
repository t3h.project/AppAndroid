package chung.com.googlevoice;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import chung.com.common.AppConstance;
import chung.com.utilities.Utilities;

/**
 * Created by root on 7/27/16.
 */
public class ActivityToLocation extends AppCompatActivity {
    private TextInputEditText edt_to_location;
    private Button btnSearch;
    private Utilities utilities;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_search_location);
        utilities= new Utilities(this);
        edt_to_location= (TextInputEditText) findViewById(R.id.edt_to_location);
        btnSearch= (Button) findViewById(R.id.btn_search_to_location);
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String location= edt_to_location.getText().toString();
                if (utilities.isConnected()&& utilities.isGPS()){
                    if (!location.isEmpty()){
                        Intent intent= new Intent();
                        intent.putExtra(AppConstance.KEY_LOCATION,location);
                        setResult(RESULT_OK,intent);
                        finish();
                    }else{
                        Toast.makeText(ActivityToLocation.this,R.string.notify_not_input_to_location,Toast.LENGTH_LONG).show();
                    }
                }else{
                    utilities.showSettingsGPSAlert();
                }


            }
        });
    }
}
