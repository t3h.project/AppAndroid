package chung.com.googlevoice;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Marker;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import chung.com.GPSTracker;
import chung.com.MapManager;
import chung.com.common.AppConstance;
import chung.com.dialog.PlaceDialogFragment;
import chung.com.model.Place;
import chung.com.utilities.Utilities;

/**
 * Created by root on 7/25/16.
 */
public class ActivityMap extends FragmentActivity implements View.OnClickListener {
    public static final int PLACE_PICKER_REQUEST = 1;
    public static final String TAG = "ActivityMap";
    private Utilities utilities;
    /**
     * Called when the activity is first created.
     */
    private MapManager mapManager;
    private FloatingActionButton fabVoice;
    private GPSTracker tracker;
    private Location curLocation;
    private String endLocaion;
    private ProgressDialog progress;

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_map);
        utilities = new Utilities(this);
        tracker = new GPSTracker(this);
        Log.e(TAG, "onCreate: true");
        initViews();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.maps);
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mapManager = new MapManager(googleMap, ActivityMap.this);
                if (savedInstanceState != null) {
                    MapManager.mHMReference.clear();

                    if (savedInstanceState.containsKey("places")
                            && savedInstanceState.containsKey("location")) {
                        Log.e(TAG, "place");

                        googleMap.setOnMyLocationChangeListener(null);
                        MapManager.mPlaces = (Place[]) savedInstanceState.getParcelableArray("places");

                        // Traversing through each near by place object
                        for (int i = 0; i < MapManager.mPlaces.length; i++) {
                            // Getting latitude and longitude of the i-th place
                            Location loc = new Location("");
                            loc.setLongitude(Double.parseDouble(MapManager.mPlaces[i].mLng));
                            loc.setLatitude(Double.parseDouble(MapManager.mPlaces[i].mLat));

                            // Drawing the marker corresponding to the i-th place
                            Marker m = mapManager.drawMarker(loc, BitmapDescriptorFactory.HUE_GREEN, "");

                            // Linkng i-th place and its marker id
                            MapManager.mHMReference.put(m.getId(), MapManager.mPlaces[i]);
                        }

                        // If a touched location is already saved
                        // Retrieving the touched location and setting in member variable
                        curLocation = savedInstanceState.getParcelable("location");

                        // Drawing a marker at the touched location
                        mapManager.drawMarker(curLocation, BitmapDescriptorFactory.HUE_ROSE, "");
//                        savedInstanceState.remove("places");
//                        savedInstanceState.remove("location");

                    } else if (savedInstanceState.containsKey("location")
                            && savedInstanceState.containsKey("endLocation")) {

                        Log.e(TAG, "endLocation");
                        endLocaion = savedInstanceState.getString("endLocation");
                        curLocation = savedInstanceState.getParcelable("location");
                        Location mLocation = mapManager.getAddressLocation(endLocaion);

                        String link = "http://maps.googleapis.com/maps/api/directions/json?"
                                + "origin=" + curLocation.getLatitude() + "," + curLocation.getLongitude()
                                + "&destination=" + mLocation.getLatitude() + "," + mLocation.getLongitude()
                                + "&sensor=false&units=metric&mode=driving";

                        CheckAddress(link, curLocation, endLocaion);
//                        savedInstanceState.remove("endLocation");
//                        savedInstanceState.remove("location");
                    }


                } else {
                    Log.e(TAG, "savedInstanceState: false");
                }
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.e(TAG, "onStart");

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // Saving all the near by places objects
        Log.e(TAG, "onSaveInstanceState");
        if (MapManager.mPlaces != null) {
            outState.putParcelableArray("places", MapManager.mPlaces);
            if (outState.containsKey("endLocation")) {
                outState.remove("endLocation");
            }
        } else {
            if (endLocaion != null) {
                outState.putString("endLocation", endLocaion);
                if (outState.containsKey("places")) {
                    outState.remove("places");
                }
            }
        }

        if (curLocation != null)
            outState.putParcelable("location", curLocation);


        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!(utilities.isConnected()) && (!utilities.isGPS())) {
            utilities.showSettingsGPSAlert();
        }
    }


    private void initViews() {
        fabVoice = (FloatingActionButton) findViewById(R.id.fab);
        fabVoice.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (!utilities.isConnected() && !utilities.isGPS()) {
            utilities.showSettingsAlert();
        } else {
            Intent speechRecognitionIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
            speechRecognitionIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault().toString());
            speechRecognitionIntent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                    getString(R.string.talking));
            startActivityForResult(speechRecognitionIntent, AppConstance.SPEECHINTENT_REQ_CODE);
//            Intent intent= new Intent(this, ActivityChooseLocation.class);
//            startActivityForResult(intent,AppConstance.REQUEST_CHOOSE_LOCATION);

        }

//        PlacePicker.IntentBuilder builder= new PlacePicker.IntentBuilder();
//
//        try {
//            Intent intent= builder.build(getApplicationContext());
//            startActivityForResult(intent,PLACE_PICKER_REQUEST);
//        } catch (GooglePlayServicesRepairableException e) {
//            e.printStackTrace();
//        } catch (GooglePlayServicesNotAvailableException e) {
//            e.printStackTrace();
//        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String result = "";
        curLocation = tracker.getLocation();
        if (curLocation == null) {
            Toast.makeText(getApplicationContext(), R.string.notify_not_get_curretn_location, Toast.LENGTH_LONG).show();
        } else {
            switch (requestCode) {
                case AppConstance.SPEECHINTENT_REQ_CODE:
                    if (resultCode == RESULT_OK) {
                        ArrayList<String> speechResults = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                        result = speechResults.get(0);
                        Log.e(TAG, result);
                        CheckTextResult(result);
                    }
                    break;
                case AppConstance.REQUEST_TO_LOCATION:
                    if (resultCode == RESULT_OK) {
                        showProgressBar();
                        endLocaion = data.getStringExtra(AppConstance.KEY_LOCATION);
                        Location mLocation = mapManager.getAddressLocation(endLocaion);

                        String link = "http://maps.googleapis.com/maps/api/directions/json?"
                                + "origin=" + curLocation.getLatitude() + "," + curLocation.getLongitude()
                                + "&destination=" + mLocation.getLatitude() + "," + mLocation.getLongitude()
                                + "&sensor=false&units=metric&mode=driving";
                        Log.e(TAG, link);

                        CheckAddress(link, curLocation, endLocaion);
                        stopProgressBar();
                    }
                    break;
                case AppConstance.REQUEST_CHOOSE_LOCATION:
                    if (resultCode == RESULT_OK) {
                        showProgressBar();
                        int mPositon = data.getIntExtra(AppConstance.KEY_POSITION, 0);
                        Log.e(TAG, "Location: " + "(" + curLocation.getLatitude() + "," + curLocation.getLongitude() + ")");
                        Log.e(TAG, "Positon: " + mPositon);
                        mapManager.showNearByPlace(mPositon, curLocation);
                        stopProgressBar();
                    }
                    break;
            }
        }
//
    }

    private void CheckTextResult(String resutl) {
        if (resutl.toUpperCase().equals("TÌM ĐỊA ĐIỂM")) {
            Intent intent = new Intent(this, ActivityToLocation.class);
            startActivityForResult(intent, AppConstance.REQUEST_TO_LOCATION);
        } else if (resutl.toUpperCase().equals("CHỌN ĐỊA ĐIỂM")) {
            Intent intent = new Intent(this, ActivityChooseLocation.class);
            startActivityForResult(intent, AppConstance.REQUEST_CHOOSE_LOCATION);
        }
    }


    public boolean CheckAddress(String url, final Location Startlocation, final String Endlocation) {

        final RequestQueue rq = Volley.newRequestQueue(this);
        final JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {
                    String status = response.getString("status");
                    if (!status.equals("OK")) {
                        Toast.makeText(getApplicationContext(), R.string.not_found_location, Toast.LENGTH_LONG).show();
                    } else {
                        mapManager.search(Startlocation, Endlocation);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {


            }
        });

        rq.add(request);
        return false;

    }

    public boolean showFragmentDialogDetail(Marker marker, HashMap<String, Place> mHMReference) {
        // If touched at User input location
        if (!mHMReference.containsKey(marker.getId()))
            return false;

        // Getting place object corresponding to the currently clicked Marker
        Place place = mHMReference.get(marker.getId());

        // Creating an instance of DisplayMetrics
        DisplayMetrics dm = new DisplayMetrics();

        // Getting the screen display metrics
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        // Creating a dialog fragment to display the photo
        PlaceDialogFragment dialogFragment = PlaceDialogFragment.newInstance(place, dm, this);

        // Getting a reference to Fragment Manager
        FragmentManager fm = getSupportFragmentManager();

        // Starting Fragment Transaction
        FragmentTransaction ft = fm.beginTransaction();

        // Adding the dialog fragment to the transaction
        ft.add(dialogFragment, "TAG");

        // Committing the fragment transaction
        ft.commit();

        return false;
    }

    private void showProgressBar() {
        progress = ProgressDialog.show(ActivityMap.this, "",
                getResources().getString(R.string.waiting_load_data), true);
    }

    private void stopProgressBar() {
        Runnable progressRunnable = new Runnable() {

            @Override
            public void run() {
                //progress.cancel();
                progress.dismiss();
            }
        };

        Handler pdCanceller = new Handler();
        pdCanceller.postDelayed(progressRunnable, 3000);
    }
}

