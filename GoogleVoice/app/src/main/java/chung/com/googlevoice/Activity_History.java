package chung.com.googlevoice;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import java.util.ArrayList;

import chung.com.adapter.RVHistory;
import chung.com.common.AppConstance;
import chung.com.data.Manager;
import chung.com.model.Call;


/**
 * Created by chung on 8/4/16.
 */
public class Activity_History extends AppCompatActivity {

    private RecyclerView RV_History;
    private RVHistory adapter;
    private ArrayList<Call> list;
    private Manager manager;
    private String phone;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Lich su cuoc goi");
        Bundle bundle = getIntent().getExtras();
        if (bundle!=null){
            phone = bundle.getString(AppConstance.KEY_PHONE);
        }
        initView();
    }

    private void initView() {
        manager = new Manager(this);
        list = new ArrayList<>();
        list.addAll(manager.getHistoryCall(phone));
        RV_History= (RecyclerView) findViewById(R.id.RV_His);
        adapter = new RVHistory(this,list);
        RV_History.setLayoutManager(new LinearLayoutManager(this));
       RV_History.setAdapter(adapter);
        adapter.notifyDataSetChanged();

    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);

    }
}
