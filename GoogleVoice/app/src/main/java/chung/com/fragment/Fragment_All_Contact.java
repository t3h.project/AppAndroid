package chung.com.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import chung.com.adapter.RecyclerViewPick;
import chung.com.data.Manager;
import chung.com.googlevoice.R;

/**
 * Created by chung on 6/27/16.
 */
public class Fragment_All_Contact extends Fragment {
    private RecyclerView _RecyclerView;
    private RecyclerViewPick adapter;
    private RecyclerView.LayoutManager layoutManager;
    private Manager manager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_all_contact, null);
        initView(view);
        return view;
    }

    private void initView(View view) {
        _RecyclerView = (RecyclerView) view.findViewById(R.id._RecyclerView);
        _RecyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        _RecyclerView.setLayoutManager(layoutManager);
//        adapter = new RecyclerViewPick(getActivity(), manager.getAllContact());
        _RecyclerView.setAdapter(adapter);
    }

}
