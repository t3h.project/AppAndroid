package chung.com.onclick;

import android.view.View;

/**
 * Created by root on 8/1/16.
 */
public interface IOnItemClickListener {
    public void onClickItem(View v, int pos);
}
