package chung.com.model;

/**
 * Created by chung on 8/3/16.
 */
public class Call {
    private int image;
    private String number;
    private String time;
    private String date;
    private String name;

    public Call(int image, String number, String time, String date, String name) {
        this.image = image;
        this.number = number;
        this.time = time;
        this.date = date;
        this.name = name;
    }

    public int getImage() {
        return image;
    }

    public String getNumber() {
        return number;
    }

    public String getTime() {
        return time;
    }

    public String getDate() {
        return date;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Call{" +
                "image=" + image +
                ", number='" + number + '\'' +
                ", time='" + time + '\'' +
                ", date='" + date + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
