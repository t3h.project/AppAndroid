package chung.com.model;

/**
 * Created by chung on 7/11/16.
 */
public class Entity {
    private String title;
    private String link;
    private String link_small;
    private String detail;

    public Entity(String title, String link, String link_small, String detail) {
        this.title = title;
        this.link = link;
        this.link_small = link_small;
        this.detail = detail;
    }

    public String getTitle() {
        return title;
    }

    public String getLink() {
        return link;
    }

    public String getLink_small() {
        return link_small;
    }

    public String getDetail() {
        return detail;
    }
}
