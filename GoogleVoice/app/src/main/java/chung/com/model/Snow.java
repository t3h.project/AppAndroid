package chung.com.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by root on 8/5/16.
 */
public class Snow implements Parcelable {
    private String time;
    private float ammount;

    public Snow(){};

    public Snow(Parcel in) {
        time = in.readString();
        ammount = in.readFloat();
    }

    public final Creator<Snow> CREATOR = new Creator<Snow>() {
        @Override
        public Snow createFromParcel(Parcel in) {
            return null;
        }

        @Override
        public Snow[] newArray(int size) {
            return new Snow[size];
        }
    };

    public String getTime() {
        return time;
    }
    public void setTime(String time) {
        this.time = time;
    }
    public float getAmmount() {
        return ammount;
    }
    public void setAmmount(float ammount) {
        this.ammount = ammount;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(time);
        parcel.writeFloat(ammount);
    }
}
