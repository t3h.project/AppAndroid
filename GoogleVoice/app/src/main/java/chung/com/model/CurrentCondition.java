package chung.com.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by root on 8/5/16.
 */
public class CurrentCondition implements Parcelable {
    private int weatherId;
    private String condition;
    private String descr;
    private String icon;
    private String date;
    private float pressure;
    private float humidity;

    public CurrentCondition(){

    }

    protected CurrentCondition(Parcel in) {
        weatherId = in.readInt();
        condition = in.readString();
        descr = in.readString();
        icon = in.readString();
        date = in.readString();
        pressure = in.readFloat();
        humidity = in.readFloat();
    }

    public static final Parcelable.Creator<CurrentCondition> CREATOR =
            new Parcelable.Creator<CurrentCondition>() {
                public CurrentCondition createFromParcel(Parcel in) {
                    return new CurrentCondition(in);
                }

                public CurrentCondition[] newArray(int size) {
                    return new CurrentCondition[size];
                }
            };

    public int getWeatherId() {
        return weatherId;
    }
    public void setWeatherId(int weatherId) {
        this.weatherId = weatherId;
    }
    public String getCondition() {
        return condition;
    }
    public void setCondition(String condition) {
        this.condition = condition;
    }
    public String getDescr() {
        return descr;
    }
    public void setDescr(String descr) {
        this.descr = descr;
    }
    public String getIcon() {
        return icon;
    }
    public void setIcon(String icon) {
        this.icon = icon;
    }
    public float getPressure() {
        return pressure;
    }
    public void setPressure(float pressure) {
        this.pressure = pressure;
    }
    public float getHumidity() {
        return humidity;
    }
    public void setHumidity(float humidity) {
        this.humidity = humidity;
    }




    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(weatherId);
        parcel.writeString(condition);
        parcel.writeString(descr);
        parcel.writeString(icon);
        parcel.writeString(date);
        parcel.writeFloat(pressure);
        parcel.writeFloat(humidity);
    }
}
