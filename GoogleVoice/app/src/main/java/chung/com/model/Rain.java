package chung.com.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by root on 8/5/16.
 */
public class Rain implements Parcelable {
    private String time;
    private float ammount;

    public Rain(){};

    public Rain(Parcel in) {
        time = in.readString();
        ammount = in.readFloat();
    }

    public final Creator<Rain> CREATOR = new Creator<Rain>() {
        @Override
        public Rain createFromParcel(Parcel in) {
            return new Rain(in);
        }

        @Override
        public Rain[] newArray(int size) {
            return new Rain[size];
        }
    };

    public String getTime() {
        return time;
    }
    public void setTime(String time) {
        this.time = time;
    }
    public float getAmmount() {
        return ammount;
    }
    public void setAmmount(float ammount) {
        this.ammount = ammount;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(time);
        parcel.writeFloat(ammount);
    }
}
