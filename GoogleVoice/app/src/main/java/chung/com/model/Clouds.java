package chung.com.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by root on 8/5/16.
 */
public class Clouds implements Parcelable {
    private int perc;

    public Clouds(){};

    public Clouds(Parcel in) {
        perc = in.readInt();
    }

    public final Creator<Clouds> CREATOR = new Creator<Clouds>() {
        @Override
        public Clouds createFromParcel(Parcel in) {
            return new Clouds(in);
        }

        @Override
        public Clouds[] newArray(int size) {
            return new Clouds[size];
        }
    };

    public int getPerc() {
        return perc;
    }

    public void setPerc(int perc) {
        this.perc = perc;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(perc);
    }
}
