package chung.com.model;

/**
 * Created by chung on 5/26/16.
 */
public class Song {
    private int id;
    private String data, title, artis, album;
    private float duration, size;

    public Song(int id, String data, String title, String artis, String album, float duration, float size) {
        this.id = id;
        this.data = data;
        this.title = title;
        this.artis = artis;
        this.album = album;
        this.duration = duration;
        this.size = size;
    }

    public int getId() {
        return id;
    }

    public String getData() {
        return data;
    }

    public String getTitle() {
        return title;
    }

    public String getArtis() {
        return artis;
    }

    public String getAlbum() {
        return album;
    }

    public float getDuration() {
        return duration;
    }

    public float getSize() {
        return size;
    }

    @Override
    public String toString() {
        return "Song{" +
                "title='" + title + '\'' +
                '}';
    }
}
