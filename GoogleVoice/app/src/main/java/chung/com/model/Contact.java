package chung.com.model;

import java.io.Serializable;

/**
 * Created by chung on 5/23/16.
 */
public class Contact implements Serializable {
    private String name;
    private String PhoneNo;

    public Contact() {
    }

    public Contact(String name, String phoneNo) {
        this.name = name;
        PhoneNo = phoneNo;
    }

    public String getName() {
        return name;
    }

    public String getPhoneNo() {
        return PhoneNo;
    }

    @Override
    public String toString() {
        return "(" + name + ")" + PhoneNo;
    }
}
