package chung.com.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by root on 8/5/16.
 */
public class Temperature implements Parcelable {
    private float temp;
    private float minTemp;
    private float maxTemp;

    public Temperature(){

    }

    public Temperature(Parcel in) {
        temp = in.readFloat();
        minTemp = in.readFloat();
        maxTemp = in.readFloat();
    }

    public static final Creator<Temperature> CREATOR = new Creator<Temperature>() {
        @Override
        public Temperature createFromParcel(Parcel in) {
            return new Temperature(in);
        }

        @Override
        public Temperature[] newArray(int size) {
            return new Temperature[size];
        }
    };

    public float getTemp() {
        return temp;
    }
    public void setTemp(float temp) {
        this.temp = temp;
    }
    public float getMinTemp() {
        return minTemp;
    }
    public void setMinTemp(float minTemp) {
        this.minTemp = minTemp;
    }
    public float getMaxTemp() {
        return maxTemp;
    }
    public void setMaxTemp(float maxTemp) {
        this.maxTemp = maxTemp;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeFloat(temp);
        parcel.writeFloat(minTemp);
        parcel.writeFloat(maxTemp);
    }
}
