package chung.com.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by root on 8/2/16.
 */
public class Weather implements Parcelable {
    public Location location;
    public CurrentCondition currentCondition;// = new CurrentCondition();
    public Temperature temperature;// = new Temperature();
    public Wind wind;// = new Wind();
    public Rain rain;// = new Rain();
    public Snow snow;// = new Snow()	;
    public Clouds clouds;// = new Clouds();

    public byte[] iconData;

    public Weather(Parcel in) {
        //location=in.
        location = in.readParcelable(Location.class.getClassLoader());
        currentCondition = in.readParcelable(CurrentCondition.class.getClassLoader());
        temperature = in.readParcelable(Temperature.class.getClassLoader());
        wind = in.readParcelable(Wind.class.getClassLoader());
        rain = in.readParcelable(Rain.class.getClassLoader());
        snow = in.readParcelable(Snow.class.getClassLoader());
        clouds = in.readParcelable(Clouds.class.getClassLoader());
        iconData = in.createByteArray();
    }

    public Weather(){

    }


    public static final Creator<Weather> CREATOR = new Creator<Weather>() {
        @Override
        public Weather createFromParcel(Parcel in) {
            return new Weather(in);
        }

        @Override
        public Weather[] newArray(int size) {
            return null;
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(location, i);
        parcel.writeParcelable(currentCondition, i);
        parcel.writeParcelable(temperature, i);
        parcel.writeParcelable(wind, i);
        parcel.writeParcelable(rain, i);
        parcel.writeParcelable(snow, i);
        parcel.writeParcelable(clouds, i);
        parcel.writeByteArray(iconData);
    }

//    public  class CurrentCondition implements Parcelable{
//        private int weatherId;
//        private String condition;
//        private String descr;
//        private String icon;
//        private String date;
//        private float pressure;
//        private float humidity;
//
//        public CurrentCondition(){
//
//        }
//
//        protected CurrentCondition(Parcel in) {
//            weatherId = in.readInt();
//            condition = in.readString();
//            descr = in.readString();
//            icon = in.readString();
//            date = in.readString();
//            pressure = in.readFloat();
//            humidity = in.readFloat();
//        }
//
//        public final Parcelable.Creator<CurrentCondition> CREATOR =
//                new Parcelable.Creator<CurrentCondition>() {
//                    public CurrentCondition createFromParcel(Parcel in) {
//                        return new CurrentCondition(in);
//                    }
//
//                    public CurrentCondition[] newArray(int size) {
//                        return null;
//                    }
//                };
//
//        public int getWeatherId() {
//            return weatherId;
//        }
//        public void setWeatherId(int weatherId) {
//            this.weatherId = weatherId;
//        }
//        public String getCondition() {
//            return condition;
//        }
//        public void setCondition(String condition) {
//            this.condition = condition;
//        }
//        public String getDescr() {
//            return descr;
//        }
//        public void setDescr(String descr) {
//            this.descr = descr;
//        }
//        public String getIcon() {
//            return icon;
//        }
//        public void setIcon(String icon) {
//            this.icon = icon;
//        }
//        public float getPressure() {
//            return pressure;
//        }
//        public void setPressure(float pressure) {
//            this.pressure = pressure;
//        }
//        public float getHumidity() {
//            return humidity;
//        }
//        public void setHumidity(float humidity) {
//            this.humidity = humidity;
//        }
//
//
//
//
//        public String getDate() {
//            return date;
//        }
//
//        public void setDate(String date) {
//            this.date = date;
//        }
//
//        @Override
//        public int describeContents() {
//            return 0;
//        }
//
//        @Override
//        public void writeToParcel(Parcel parcel, int i) {
//            parcel.writeInt(weatherId);
//            parcel.writeString(condition);
//            parcel.writeString(descr);
//            parcel.writeString(icon);
//            parcel.writeString(date);
//            parcel.writeFloat(pressure);
//            parcel.writeFloat(humidity);
//        }
//    }

//    public  class Temperature implements Parcelable{
//        private float temp;
//        private float minTemp;
//        private float maxTemp;
//
//        public Temperature(){
//
//        }
//
//        protected Temperature(Parcel in) {
//            temp = in.readFloat();
//            minTemp = in.readFloat();
//            maxTemp = in.readFloat();
//        }
//
//        public final Creator<Temperature> CREATOR = new Creator<Temperature>() {
//            @Override
//            public Temperature createFromParcel(Parcel in) {
//                return new Temperature(in);
//            }
//
//            @Override
//            public Temperature[] newArray(int size) {
//                return null;
//            }
//        };
//
//        public float getTemp() {
//            return temp;
//        }
//        public void setTemp(float temp) {
//            this.temp = temp;
//        }
//        public float getMinTemp() {
//            return minTemp;
//        }
//        public void setMinTemp(float minTemp) {
//            this.minTemp = minTemp;
//        }
//        public float getMaxTemp() {
//            return maxTemp;
//        }
//        public void setMaxTemp(float maxTemp) {
//            this.maxTemp = maxTemp;
//        }
//
//        @Override
//        public int describeContents() {
//            return 0;
//        }
//
//        @Override
//        public void writeToParcel(Parcel parcel, int i) {
//            parcel.writeFloat(temp);
//            parcel.writeFloat(minTemp);
//            parcel.writeFloat(maxTemp);
//        }
//    }

//    public  class Wind implements Parcelable{
//        private float speed;
//        private float deg;
//
//        public Wind(){
//
//        }
//
//        protected Wind(Parcel in) {
//            speed = in.readFloat();
//            deg = in.readFloat();
//        }
//
//        public final Creator<Wind> CREATOR = new Creator<Wind>() {
//            @Override
//            public Wind createFromParcel(Parcel in) {
//                return new Wind(in);
//            }
//
//            @Override
//            public Wind[] newArray(int size) {
//                return null;
//            }
//        };
//
//        public float getSpeed() {
//            return speed;
//        }
//        public void setSpeed(float speed) {
//            this.speed = speed;
//        }
//        public float getDeg() {
//            return deg;
//        }
//        public void setDeg(float deg) {
//            this.deg = deg;
//        }
//
//
//        @Override
//        public int describeContents() {
//            return 0;
//        }
//
//        @Override
//        public void writeToParcel(Parcel parcel, int i) {
//            parcel.writeFloat(speed);
//            parcel.writeFloat(deg);
//        }
//    }

//    public  class Rain implements Parcelable{
//        private String time;
//        private float ammount;
//
//        public Rain(){};
//
//        protected Rain(Parcel in) {
//            time = in.readString();
//            ammount = in.readFloat();
//        }
//
//        public final Creator<Rain> CREATOR = new Creator<Rain>() {
//            @Override
//            public Rain createFromParcel(Parcel in) {
//                return new Rain(in);
//            }
//
//            @Override
//            public Rain[] newArray(int size) {
//                return null;
//            }
//        };
//
//        public String getTime() {
//            return time;
//        }
//        public void setTime(String time) {
//            this.time = time;
//        }
//        public float getAmmount() {
//            return ammount;
//        }
//        public void setAmmount(float ammount) {
//            this.ammount = ammount;
//        }
//
//
//        @Override
//        public int describeContents() {
//            return 0;
//        }
//
//        @Override
//        public void writeToParcel(Parcel parcel, int i) {
//            parcel.writeString(time);
//            parcel.writeFloat(ammount);
//        }
//    }
//
//    public  class Snow implements Parcelable{
//        private String time;
//        private float ammount;
//
//        public Snow(){};
//
//        protected Snow(Parcel in) {
//            time = in.readString();
//            ammount = in.readFloat();
//        }
//
//        public final Creator<Snow> CREATOR = new Creator<Snow>() {
//            @Override
//            public Snow createFromParcel(Parcel in) {
//                return null;
//            }
//
//            @Override
//            public Snow[] newArray(int size) {
//                return null;
//            }
//        };
//
//        public String getTime() {
//            return time;
//        }
//        public void setTime(String time) {
//            this.time = time;
//        }
//        public float getAmmount() {
//            return ammount;
//        }
//        public void setAmmount(float ammount) {
//            this.ammount = ammount;
//        }
//
//
//        @Override
//        public int describeContents() {
//            return 0;
//        }
//
//        @Override
//        public void writeToParcel(Parcel parcel, int i) {
//            parcel.writeString(time);
//            parcel.writeFloat(ammount);
//        }
//    }
//
//    public  class Clouds implements Parcelable{
//        private int perc;
//
//        public Clouds(){};
//
//        protected Clouds(Parcel in) {
//            perc = in.readInt();
//        }
//
//        public final Creator<Clouds> CREATOR = new Creator<Clouds>() {
//            @Override
//            public Clouds createFromParcel(Parcel in) {
//                return new Clouds(in);
//            }
//
//            @Override
//            public Clouds[] newArray(int size) {
//                return null;
//            }
//        };
//
//        public int getPerc() {
//            return perc;
//        }
//
//        public void setPerc(int perc) {
//            this.perc = perc;
//        }
//
//
//        @Override
//        public int describeContents() {
//            return 0;
//        }
//
//        @Override
//        public void writeToParcel(Parcel parcel, int i) {
//            parcel.writeInt(perc);
//        }
//    }
}
