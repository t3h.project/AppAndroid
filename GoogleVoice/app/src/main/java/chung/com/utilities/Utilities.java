package chung.com.utilities;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Parcelable;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;


import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import chung.com.common.AppConstance;
import chung.com.googlevoice.R;


public class Utilities {
    private Context mContext;

    public Utilities(Context mContext) {
        super();
        this.mContext = mContext;
    }


    /**
     * *******************************************************************************************************
     */
    //Check network status
    public boolean isConnected() {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnected()) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isGPS() {
        LocationManager manager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            return true;
        } else {
            return false;
        }
    }

    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);

        alertDialog.setTitle("Network").setCancelable(false);

        alertDialog.setMessage("Network is not enabled. Do you want to go to settings menu?");

        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_SETTINGS);
                mContext.startActivity(intent);
                dialog.dismiss();
            }
        });

        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        alertDialog.show();
    }

    public void showSettingsGPSAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);

        alertDialog.setTitle("Network/GPS ").setCancelable(false);

        alertDialog.setMessage("Network/GPS is not enabled. Do you want to go to settings menu?");

        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                mContext.startActivity(intent);
                dialog.dismiss();
            }
        });

        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alertDialog.show();
    }

    //Check device
    public String typeOfDevice() {
        String device = "";
        TelephonyManager manager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
        if (manager.getPhoneType() == TelephonyManager.PHONE_TYPE_NONE) {
            device = "TABLET";
        } else {
            device = "MOBILE";
        }
        return device;
    }

    /**
     * *********************************************************************************************************
     */

    //Share Link to social network
    public void share_link(String fileShare) {

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, fileShare);
        // intent.putExtra(android.content.Intent.EXTRA_STREAM,fileShare);
        ((Activity) mContext).startActivityForResult(Intent.createChooser(intent,
                "Chọn ứng dụng cần chia sẻ"), 100);
    }

    /**
     * *********************************************************************************************************
     */
    //Convert milisecond to time - format H:i:s
    public String milisecondToTime(long milisecond) {
        long second = (milisecond / 1000) % 60;
        long minute = (milisecond / (1000 * 60)) % 60;
        long hour = (milisecond / (1000 * 60 * 60)) % 24;

        String time = String.format("%02d:%02d:%02d", hour, minute, second);
        return time;
    }

    /**
     * *********************************************************************************************************
     */
    //Canculate Date
    public static Date currentDate() {
        Calendar calendar = Calendar.getInstance();
        return calendar.getTime();
    }

    public String convertRainCode(int code) {
        String rain = "";
        switch (code) {
            case 500:
                rain = mContext.getResources().getString(R.string.light_rain);
                break;
            case 501:
            case 520:
            case 521:
            case 522:
            case 531:
                rain = mContext.getResources().getString(R.string.moderate_rain);
                break;
            case 502:
            case 503:
            case 504:
                rain = mContext.getResources().getString(R.string.heavy_rain);
                break;
            case 511:
                rain = mContext.getResources().getString(R.string.freezing_rain);
                break;
        }

        return rain;
    }

    public boolean compareToAfterDate(String dCurrent, String dAfter) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date1 = sdf.parse(dCurrent);
        Date date2 = sdf.parse(dAfter);

        if (date2.after(date1)){
            return true;
        }
        else{
            return false;
        }
    }

    public String formatTime(String date){
        String result= date.substring(date.indexOf(" ")+1,date.length()-3);
        return result;

    }
    public String fomatDate(String date) throws ParseException {
        SimpleDateFormat format= new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date newDate= format.parse(date);
        format= new SimpleDateFormat("EEE",Locale.ENGLISH);
        return format.format(newDate);
    }

    public String formatDateString(String date) throws ParseException {
        SimpleDateFormat format= new SimpleDateFormat("yyyy-MM-dd");
        Date newDate= format.parse(date);
        format= new SimpleDateFormat("EEE, MMM dd",Locale.ENGLISH);
        return format.format(newDate);
    }

    public boolean compareToEqualTime(String d1, String d2) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date date1 = sdf.parse(d1);
        Date date2 = sdf.parse(d2);
        if (date1.equals(date2)){
            return true;
        }else{
            return false;
        }
    }

    public String convertDegreeToDirection(float degree) {
        String direction = "";
        if (degree <= 11.25 || degree > 348.75) {
            direction = AppConstance.NORTH;
        } else if (degree < 78.75) {
            direction = AppConstance.NORT_EAST;
        } else if (degree < 101.25) {
            direction = AppConstance.EAST;
        } else if (degree < 168.75) {
            direction = AppConstance.EAST_SOUTH;
        } else if (degree < 191.25) {
            direction = AppConstance.SOUTH;
        } else if (degree < 258.75) {
            direction = AppConstance.SOUTH_WEST;
        } else if (degree < 281.25) {
            direction = AppConstance.WEST;
        } else if (degree < 348.75) {
            direction = AppConstance.WEST_NORTH;
        }
        return direction;
    }

    public int imageDrawable(String icon) {
        int drawable = 0;
        switch (icon) {
            case "01d":
                drawable=R.drawable.icon_01d;
                break;
            case "01n":
                drawable=R.drawable.icon_01n;
                break;
            case "02d":
                drawable=R.drawable.icon_02d;
                break;
            case "02n":
                drawable=R.drawable.icon_02n;
                break;
            case "03d":
                drawable=R.drawable.icon_03d;
                break;
            case "03n":
                drawable=R.drawable.icon_03n;
                break;
            case "04d":
                drawable=R.drawable.icon_04d;
                break;
            case "04n":
                drawable=R.drawable.icon_04n;
                break;
            case "09d":
                drawable=R.drawable.icon_09d;
                break;
            case "09n":
                drawable=R.drawable.icon_09n;
                break;
            case "10d":
                drawable=R.drawable.icon_10d;
                break;
            case "10n":
                drawable=R.drawable.icon_10n;
                break;
            case "11d":
            case "11n":
                drawable=R.drawable.icon_11d;
                break;
            case "13d":
            case "13n":
                drawable=R.drawable.icon_13d;
                break;
            case "50d":
                drawable=R.drawable.icon_50n;
                break;
            case "50n":
                drawable=R.drawable.icon_50n;
                break;

        }
        return drawable;
    }

//    public static String getTimeAgo(Date date, Context ctx) {
//
//        if (date == null) {
//            return null;
//        }
//
//        long time = date.getTime();
//
//        Date curDate = currentDate();
//        long now = curDate.getTime();
//        if (time > now || time <= 0) {
//            return null;
//        }
//
//        int dim = getTimeDistanceInMinutes(time);
//
//        String timeAgo = null;
//
//        if (dim == 0) {
//            timeAgo = ctx.getResources().getString(R.string.date_util_term_less) + " " + ctx.getResources().getString(R.string.date_util_term_a) + " " + ctx.getResources().getString(R.string.date_util_unit_minute);
//        } else if (dim == 1) {
//            return "1 " + ctx.getResources().getString(R.string.date_util_unit_minute);
//        } else if (dim >= 2 && dim <= 44) {
//            timeAgo = dim + " " + ctx.getResources().getString(R.string.date_util_unit_minutes);
//        } else if (dim >= 45 && dim <= 89) {
//            timeAgo = ctx.getResources().getString(R.string.date_util_prefix_about) + " " + ctx.getResources().getString(R.string.date_util_term_an) + " " + ctx.getResources().getString(R.string.date_util_unit_hour);
//        } else if (dim >= 90 && dim <= 1439) {
//            timeAgo = ctx.getResources().getString(R.string.date_util_prefix_about) + " " + (Math.round(dim / 60)) + " " + ctx.getResources().getString(R.string.date_util_unit_hours);
//        } else if (dim >= 1440 && dim <= 2519) {
//            timeAgo = "1 " + ctx.getResources().getString(R.string.date_util_unit_day);
//        } else if (dim >= 2520 && dim <= 43199) {
//            timeAgo = (Math.round(dim / 1440)) + " " + ctx.getResources().getString(R.string.date_util_unit_days);
//        } else if (dim >= 43200 && dim <= 86399) {
//            timeAgo = ctx.getResources().getString(R.string.date_util_prefix_about) + " " + ctx.getResources().getString(R.string.date_util_term_a) + " " + ctx.getResources().getString(R.string.date_util_unit_month);
//        } else if (dim >= 86400 && dim <= 525599) {
//            timeAgo = (Math.round(dim / 43200)) + " " + ctx.getResources().getString(R.string.date_util_unit_months);
//        } else if (dim >= 525600 && dim <= 655199) {
//            timeAgo = ctx.getResources().getString(R.string.date_util_prefix_about) + " " + ctx.getResources().getString(R.string.date_util_term_a) + " " + ctx.getResources().getString(R.string.date_util_unit_year);
//        } else if (dim >= 655200 && dim <= 914399) {
//            timeAgo = ctx.getResources().getString(R.string.date_util_prefix_over) + " " + ctx.getResources().getString(R.string.date_util_term_a) + " " + ctx.getResources().getString(R.string.date_util_unit_year);
//        } else if (dim >= 914400 && dim <= 1051199) {
//            timeAgo = ctx.getResources().getString(R.string.date_util_prefix_almost) + " 2 " + ctx.getResources().getString(R.string.date_util_unit_years);
//        } else {
//            timeAgo = ctx.getResources().getString(R.string.date_util_prefix_about) + " " + (Math.round(dim / 525600)) + " " + ctx.getResources().getString(R.string.date_util_unit_years);
//        }
//
//        return timeAgo + " " + ctx.getResources().getString(R.string.date_util_suffix);
//    }

    private static int getTimeDistanceInMinutes(long time) {
        long timeDistance = currentDate().getTime() - time;
        return Math.round((Math.abs(timeDistance) / 1000) / 60);
    }

    /**
     * *************************************************************************************************
     */
//    public void Share(Activity activity, int keyShare) {
//        List<Intent> targetShareIntents = new ArrayList<Intent>();
//        Intent shareIntent = new Intent();
//        shareIntent.setAction(Intent.ACTION_SEND);
//        shareIntent.setType("text/plain");
//        List<ResolveInfo> resInfos = activity.getPackageManager().queryIntentActivities(shareIntent, 0);
//        if (!resInfos.isEmpty()) {
//            for (ResolveInfo resInfo : resInfos) {
//                String packageName = resInfo.activityInfo.packageName;
//                if (packageName.contains("com.facebook.katana")) {
//                    Intent intent = new Intent();
//                    intent.setComponent(new ComponentName(packageName, resInfo.activityInfo.name));
//                    intent.setAction(Intent.ACTION_SEND);
//                    intent.setType("text/plain");
//                    intent.putExtra(Intent.EXTRA_TEXT, AppConstants.APP_LINK);
//                    intent.setPackage(packageName);
//                    targetShareIntents.add(intent);
//                }
//                if (packageName.contains("com.google.android.gm")) {
//                    Intent intent = new PlusShare.Builder(activity)
//                            .setType("text/plain")
//                            .setText("Welcome to KQTT")
//                            .setContentUrl(Uri.parse(AppConstants.APP_LINK))
//                            .getIntent();
//                    targetShareIntents.add(intent);
//                }
//            }
//            if (!targetShareIntents.isEmpty()) {
//                Intent chooserIntent = Intent.createChooser(targetShareIntents.remove(0), "Chọn ứng dụng cần chia sẻ");
//                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetShareIntents.toArray(new Parcelable[]{}));
//                activity.startActivityForResult(chooserIntent, keyShare);
//            } else {
//                Toast.makeText(mContext, mContext.getResources().getString(R.string.no_app_share), Toast.LENGTH_SHORT).show();
//            }
//        }
//
//    }

    //    public static String md5(String s) {
//        try {
//
//            // Create MD5 Hash
//            MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
//            digest.update(s.getBytes());
//            byte messageDigest[] = digest.digest();
//
//            // Create Hex String
//            StringBuffer hexString = new StringBuffer();
//            for (int i = 0; i < messageDigest.length; i++)
//                hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
//            return hexString.toString();
//
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//        }
//        return "";
//
//    }
    public static String md5(String pass) {
        String password = null;
        MessageDigest mdEnc;
        try {
            mdEnc = MessageDigest.getInstance("MD5");
            mdEnc.update(pass.getBytes(), 0, pass.length());
            pass = new BigInteger(1, mdEnc.digest()).toString(16);
            while (pass.length() < 32) {
                pass = "0" + pass;
            }
            password = pass;
        } catch (NoSuchAlgorithmException e1) {
            e1.printStackTrace();
        }
        return password;
    }

    public boolean getSpecialCharacterCount(String s) {
        Pattern p = Pattern.compile("[^A-Za-z0-9]");
        Matcher m = p.matcher(s);
        return m.find();
    }

    public String convertISODateToDate(String s) {
        String strCreHour = s.substring(s.indexOf("T") + 1, s.indexOf("."));
        String strCreDay = s.substring(0, s.indexOf("T"));
        return strCreHour + " " + strCreDay;
    }


}